//
//  SightingRecord.swift
//  Carnivore Tracker
//
//  Created by Eric Berman on 7/11/16.
//  Copyright © 2016 Cheetah Conservation Fund. All rights reserved.
//

import Foundation

class SightingRecord {
    var timeStamp : NSDate?
    var count : String?
    var notes : String?
    var species : String?
    
    init(timeStamp: NSDate?, species : String?, count: String?, notes: String?) {
        self.timeStamp = timeStamp
        self.count = count
        self.notes = notes
        self.species = species
    }
}