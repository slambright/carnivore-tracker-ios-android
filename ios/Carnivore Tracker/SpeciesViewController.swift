//
//  SpeciesViewController.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 3/8/15.
//  Copyright (c) 2016 Cheetah Conservation Fund. All rights reserved.
//

import CoreLocation
import UIKit

class SpeciesViewController: UITableViewController, UITextFieldDelegate, UIAlertViewDelegate {
    var options : [NSString] = [ "1", "2", "3", "4", "5",
    "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20+"]

    // Interface-builder defined table cells:
    @IBOutlet weak var tcImage : UITableViewCell!
    @IBOutlet weak var tcNotes : UITableViewCell!
    @IBOutlet weak var tcCount : UITableViewCell!
    @IBOutlet weak var lblNotes : UILabel!
    @IBOutlet weak var lblCount : UILabel!
    
    // And Inteface-builder defined controls:
    @IBOutlet weak var tvNotes : UITextField!
    @IBOutlet weak var imgImage : UIImageView!
    @IBOutlet weak var individualsDownPicker : UIDownPicker!
    
    // Public properties
    var canReportSighting : Bool = false
    var speciesData : SpeciesData?
    var individualsPickerData = NSMutableArray()
    var collectionView : SpeciesCollectionViewController? = nil
    var sightingsCount : String?
    var freeText : String?

    var fileMgr: NSFileManager = NSFileManager.defaultManager()
    
    var locMgr = CLLocationManager()
    var gpsCoord : CLLocation? = nil
    
    // Useful symbolic constants
    enum sectionNames :Int {
        case sectSpeciesDescription = 0
        case sectSightingData = 1
        case sectSubmit = 2
        case sectMap = 3}
    
    enum rowDescriptions :Int {
        case rowImage = 0
        case rowName = 1
        case rowOtherName = 2
        case rowIUCN = 3
        case rowHabitat = 4
        case rowDiet = 5
        case rowActivity = 6
        case rowSize = 7
        case rowWeight = 8
        case rowSocialBehavior = 9
    }

    enum rowDataEntry :Int {
        case rowNumberSeen = 0
        case rowNotes = 1
    }

    // swift seems to have no good way to count the number of items in an enum.  Lame.  So store those counts
    let cSections = 4   // 4 sections: species information, user data, a submit button and a map
    let cRowsSpeciesDescription = 10
    let cRowsSightingData = 2

    // MARK: - UITableView data source functions
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return canReportSighting ? cSections : 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sect = sectionNames(rawValue: section)!
        switch (sect) {
        case sectionNames.sectSpeciesDescription:
            return cRowsSpeciesDescription
        case sectionNames.sectSightingData:
            return cRowsSightingData
        case sectionNames.sectSubmit:
            return 1
        case sectionNames.sectMap:
            return 1
        }
    }
    
    func descriptionCell(tableView:UITableView, label: String, value: NSString) -> UITableViewCell {
        let identifier = "descriptionCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier)
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: identifier)
        }

        cell!.textLabel?.text = label
        cell!.textLabel?.numberOfLines = 1
        cell!.textLabel?.font = UIFont.boldSystemFontOfSize(16.0)
        cell!.detailTextLabel?.text = value as String
        cell!.detailTextLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        cell!.detailTextLabel?.numberOfLines = 4
        cell!.backgroundColor = UIColor.clearColor()
        cell!.backgroundView = UIView(frame: CGRect.zero)
        cell!.selectionStyle = UITableViewCellSelectionStyle.None
        return cell!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let sect = sectionNames(rawValue: indexPath.section)!
        switch (sect) {
        case sectionNames.sectSpeciesDescription:
            switch (rowDescriptions(rawValue: indexPath.row)!) {
            case rowDescriptions.rowImage:
                let imagePath = NSBundle.mainBundle().pathForResource(self.speciesData!.image as String, ofType:"jpg") as String?
                if (imagePath != nil) {
                    self.imgImage.image = UIImage(contentsOfFile: imagePath!)
                }
                self.imgImage.contentMode = UIViewContentMode.ScaleAspectFit
                self.tcImage.backgroundColor = self.tcImage.backgroundColor // see http://stackoverflow.com/questions/27551291/uitableview-backgroundcolor-always-white-on-ipad for why this needs to be done on iPad.  Lame.
                return self.tcImage
            case rowDescriptions.rowDiet:
                return descriptionCell(tableView, label:NSLocalizedString("label_species_diet", comment: "Label for the diet of the species"), value: self.speciesData!.diet)
            case rowDescriptions.rowHabitat:
                return descriptionCell(tableView, label:NSLocalizedString("label_species_habitat", comment: "Label for the habitat of the species"), value: self.speciesData!.habitat)
            case rowDescriptions.rowIUCN:
                return descriptionCell(tableView, label:NSLocalizedString("label_iucn_status", comment: "Label for the IUCN status for the species"), value: self.speciesData!.iucnStatus)
            case rowDescriptions.rowName:
                return descriptionCell(tableView, label:self.speciesData!.speciesName as String, value: self.speciesData!.interestingFact)
            case rowDescriptions.rowActivity:
                return descriptionCell(tableView, label:NSLocalizedString("label_activity", comment: "Label for the active time of the species"), value: self.speciesData!.activity)
            case rowDescriptions.rowSize:
                return descriptionCell(tableView, label:NSLocalizedString("label_size", comment: "Label for the size of the species"), value: self.speciesData!.size)
            case rowDescriptions.rowWeight:
                return descriptionCell(tableView, label:NSLocalizedString("label_weight", comment: "Label for the weight of the species"), value: self.speciesData!.weight)
            case rowDescriptions.rowSocialBehavior:
                return descriptionCell(tableView, label:NSLocalizedString("label_social_behavior", comment: "Label for the social behavior of the species"), value: self.speciesData!.socialBehavior)
            case rowDescriptions.rowOtherName:
                return descriptionCell(tableView, label:NSLocalizedString("label_species_other_names", comment: "Label for alternative names for the species"), value: self.speciesData!.altNames)
            }
        case sectionNames.sectSightingData:
            switch (rowDataEntry(rawValue: indexPath.row)!)
            {
            case rowDataEntry.rowNotes:
                self.tcNotes.backgroundColor = self.tcNotes.backgroundColor
                return self.tcNotes
            case rowDataEntry.rowNumberSeen:
                self.tcCount.backgroundColor = self.tcCount.backgroundColor
                return self.tcCount
            }
        case sectionNames.sectSubmit:
            let cell = UITableViewCell()
            cell.textLabel?.text = NSLocalizedString("button_report_sighting", comment: "Button to report a sighting")
            cell.imageView?.image = UIImage(contentsOfFile: NSBundle.mainBundle().pathForResource("report sighting paw", ofType:"png")!)
            cell.backgroundColor = tableView.backgroundColor
            return cell
        case sectionNames.sectMap:
            let cell = UITableViewCell()
            cell.textLabel?.text = NSLocalizedString("button_open_map", comment: "Button to open a map of sightings")
            cell.backgroundColor = tableView.backgroundColor
            return cell
        }
    }
    
    // MARK: - UITableView delegate functions
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (indexPath.section == sectionNames.sectSpeciesDescription.rawValue &&
            indexPath.row == rowDescriptions.rowName.rawValue) {
            return 75;
        }
        if (indexPath.section == sectionNames.sectSpeciesDescription.rawValue && indexPath.row == rowDescriptions.rowImage.rawValue) {
            return self.tcImage.frame.height;
        }
        else if (indexPath.section == sectionNames.sectSightingData.rawValue) {
            if (indexPath.row == rowDataEntry.rowNotes.rawValue) {
                return tcNotes.frame.height
            }
            if (indexPath.row == rowDataEntry.rowNumberSeen.rawValue) {
                return tcCount.frame.height
            }
        }
        return self.tableView.rowHeight;
    }

     override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == sectionNames.sectSubmit.rawValue) {
            onReportSighting()
        }
        else if (indexPath.section == sectionNames.sectMap.rawValue) {
            let baseUrl = "https://public.tableau.com/views/CarnivoreTrackerReportedSightings/CarnivoreTrackerSightings?:embed=y&:display_count=no&:showTabs=n&Species=";
            let urlStr = (baseUrl as String) + (self.speciesData!.speciesName as String).stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
            let url = NSURL(string:urlStr)

            if (url != nil)
            {
                UIApplication.sharedApplication().openURL(url!)
            }
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return false;
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.individualsDownPicker.DownPicker = DownPicker(textField: self.individualsDownPicker, withData: NSMutableArray(array:options))
        self.individualsDownPicker.text = options[0] as String
        self.tvNotes.text = ""
        
        // localize at runtime.
        self.lblCount.text = NSLocalizedString("label_number_of_individuals", comment: "Label for the place to enter the number of individuals that were spotted")
        self.lblNotes.text = NSLocalizedString("label_free_text", comment: "Label for free-form notes")
        
        // TODO: This is a hack - why aren't delegate/backgroundcolor being set in the storyboard?
        self.tableView.backgroundColor = self.tcCount.backgroundColor
        
        locMgr.desiredAccuracy =
            kCLLocationAccuracyKilometer
        locMgr.delegate = self
        if #available(iOS 8.0, *) {
            if (locMgr.respondsToSelector(#selector(CLLocationManager.requestWhenInUseAuthorization))) {
                    locMgr.requestWhenInUseAuthorization()
            }
            else {
                locMgr.startUpdatingLocation()
            }
        } else {
            // Fallback on earlier versions
            locMgr.startUpdatingLocation()
        }
    }
    
    func onReportSighting() {
        if (gpsCoord == nil) {
            gpsCoord = collectionView?.gpsCoord
        }

        if (gpsCoord == nil) {
            let alert = UIAlertView(
                title: NSLocalizedString("title_err_no_gps", comment: "Title of error when no GPS coordinate is available"),
                message: NSLocalizedString("err_no_gps", comment: "Error message when no GPS coordinate is available"),
                delegate: self,
                cancelButtonTitle: NSLocalizedString("button_Cancel", comment: "Generic Cancel button"),
                otherButtonTitles: NSLocalizedString("button_OK", comment: "Generic OK button"))
            alert.show()
        }
        else {
            recordSighting()
        }
    }

    func recordSighting() {
        // Add to the offline sightings CSV file
        //   and then upload
        let dataFile = AppDelegate.sightingsFileName()
        let localDataFile = AppDelegate.localSightingsFileName()
        let rowData = sightingDataRow()
        
        var reportingsContents = NSString()
        if fileMgr.fileExistsAtPath(dataFile) {
            let fileBuffer = fileMgr.contentsAtPath(dataFile)
            reportingsContents = NSString(data: fileBuffer!,
                encoding: NSUTF8StringEncoding)!
        }
        
        reportingsContents = (reportingsContents as String) + rowData
        
        var historyContents = NSString()
        if fileMgr.fileExistsAtPath(localDataFile) {
            let fileBuffer = fileMgr.contentsAtPath(localDataFile)
            historyContents = NSString(data: fileBuffer!, encoding: NSUTF8StringEncoding)!
        }
        historyContents = (historyContents as String) + rowData
        
        do {
            try reportingsContents.writeToFile(dataFile, atomically: true, encoding: NSUTF8StringEncoding)
            try historyContents.writeToFile(localDataFile, atomically: true, encoding: NSUTF8StringEncoding)
        } catch _ {
        }
        
        navigationController?.popViewControllerAnimated(true)
        if (collectionView  != nil) {
            collectionView!.uploadSightings(true)
        }
    }
    
    // Remove non-ASCII text, any newlines, or any CSV-sensitive character.
    func cleanText(text:String) -> String {
        // Now clean up the free text and add it.  A bit of a hack...
        var cleanedText = String()
        for ch in (self.tvNotes.text?.unicodeScalars)! {
            let code = ch.value
            if (code < 32 || code > 255 || ch == "," || ch == ";" || ch == "\"" || ch == "'") {
                cleanedText.appendContentsOf("_")
                continue
            }
            cleanedText.append(ch)
        }
        return cleanedText
    }
    
    func sightingDataRow() -> String {
        var newDataRow = "<SOL>,"
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC");
        
        newDataRow += dateFormatter.stringFromDate(date) + ","
        
        newDataRow += (self.speciesData!.speciesName as String) + ","
        //newDataRow += options[individualsPicker.selectedRowInComponent(0)] + ","
        newDataRow += self.individualsDownPicker.text! + ","
        
        newDataRow += gpsCoord!.coordinate.latitude.description + ","
        newDataRow += gpsCoord!.coordinate.longitude.description + ","
        
        
        // iOS definitely doesn't have altitude -> YES IT DOES
        newDataRow += (gpsCoord == nil ? "0" : (gpsCoord?.altitude.description)!) + ","
        
        newDataRow += cleanText(self.tvNotes.text!) + "\n";
        
        return newDataRow
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SpeciesViewController: UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return options.count
    }
}

extension SpeciesViewController: UIPickerViewDelegate {
    func pickerView(pickerView: UIPickerView,
        rowHeightForComponent component: Int) -> CGFloat
    {
        let sysFont = UIFont.systemFontOfSize(UIFont.systemFontSize())
        let size = options[component].sizeWithAttributes([NSFontAttributeName: sysFont])
        return size.height
    }
    
    func pickerView(pickerView: UIPickerView,
        titleForRow row: Int,
        forComponent component: Int) -> String?
    {
        return options[row] as String
    }
}

extension SpeciesViewController : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locMgr.stopUpdatingLocation()
        print(error)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        gpsCoord = locationObj
        collectionView!.gpsCoord = gpsCoord
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
            case CLAuthorizationStatus.Restricted:
                NSLog("GPS auth restricted")
                break
            case CLAuthorizationStatus.Denied:
                NSLog("GPS auth denied")
                break
            case CLAuthorizationStatus.NotDetermined:
                NSLog("GPS auth not determined")
                break
            default:
                locMgr.startUpdatingLocation()
        }
    }
}
