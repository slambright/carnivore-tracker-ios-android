//
//  SpeciesData.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 3/1/15.
//  Copyright (c) 2016 Cheetah Conservation Fund. All rights reserved.
//

import UIKit

class SpeciesData: NSObject {
    var speciesName = NSString()
    var habitat = NSString()
    var diet = NSString()
    var altNames = NSString()
    var image = NSString()
    var iucnStatus = NSString()
    var activity = NSString()
    var size = NSString()
    var weight = NSString()
    var socialBehavior = NSString()
    var interestingFact = NSString()
    
    init(speciesName: String, altNames: String, iucnStatus: String, habitat: String, diet: String, size: String, weight: String, activity: String, socialBehavior: String, interestingFact: String, image: String) {
        self.speciesName = speciesName
        self.habitat = habitat
        self.diet = diet
        self.altNames = altNames
        self.iucnStatus = iucnStatus
        self.size = size
        self.weight = weight
        self.activity = activity
        self.socialBehavior = socialBehavior
        self.interestingFact = interestingFact
        self.image = image
    }

    func loadCell(cell: SpeciesCollectionViewCell)
    {
        cell.loadData(speciesName, image: image)
    }
    
    static func loadSpecies() -> [SpeciesData] {
        let dataFileName = NSLocalizedString("file_species_data", comment: "The name of the CSV file that has the species data in it.")
        
        let csvSourcePath = NSBundle.mainBundle().pathForResource(dataFileName, ofType: "csv")
        
        var result = [SpeciesData]()
        var csvData = ""
        
        do {
            try csvData = NSString(contentsOfFile: csvSourcePath!, encoding: NSUTF8StringEncoding) as String
        }
        catch _ { }
        
        let parser = CSVToArrays()
        parser.parseFromCSV(csvData as String)
        var firstRowSkipped = false
        
        for row in parser.parsedRows {
            if (firstRowSkipped == false)
            {
                firstRowSkipped = true;
            }
            else
            {
                result.append(
                    SpeciesData(
                        speciesName: row[0],
                        altNames: row[1],
                        iucnStatus: row[2],
                        habitat: row[3],
                        diet: row[4],
                        size: row[5],
                        weight: row[6],
                        activity: row[7],
                        socialBehavior: row[8],
                        interestingFact: row[9],
                        image: row[10]))
            }
        }
        
        return result;
    }
}
