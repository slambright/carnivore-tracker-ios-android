//
//  EditAccountViewController.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 3/29/15.
//  Copyright (c) 2015 Cheetah Conservation Fund. All rights reserved.
//

import UIKit

class EditAccountViewController: UIViewController {

    @IBOutlet var emailAddress: UITextField!
    @IBOutlet var currentPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    @IBOutlet var confirmNewPassword: UITextField!
    @IBOutlet var phone: UITextField!
    var collectionView : SpeciesCollectionViewController? = nil
    var test : DownPicker? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        populate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populate() {
        if (emailAddress != nil && collectionView != nil) {
            let collectionView = self.collectionView!
            emailAddress.text = currentEmail()
            phone.text = collectionView.phone
        }
    }
    
    func currentEmail() -> String {
        if (collectionView != nil && collectionView!.viewController != nil &&
            collectionView!.viewController!.firebaseRoot != nil &&
            collectionView!.viewController!.firebaseRoot!.authData != nil) {
                let firebase = collectionView!.viewController!.firebaseRoot!
                let authData = firebase.authData
                let currentEmail = authData.providerData["email"] as! String
                return currentEmail
        }
        return ""
    }
    
    @IBAction func updateAccountRequested(sender: AnyObject) {
        
        if (validateInputs()) {
            // If nothing changed, just return
            if (currentEmail() == emailAddress.text && newPassword.text == "" && phone.text == collectionView!.phone) {
                navigationController!.popViewControllerAnimated(true)
            }
            else {
                // Apply updates (or at least try to)
                updatePhase1()
            }
        }
    }
    
    // First update: email address
    func updatePhase1() {
        // Update email
        if (emailAddress.text != currentEmail()) {
            let firebase = collectionView!.viewController!.firebaseRoot!
            firebase.changeEmailForUser(currentEmail(), password: currentPassword.text, toNewEmail: emailAddress.text, withCompletionBlock: { error
                    in
                if error != nil {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(
                            title: NSLocalizedString("title_err_email_update_failed", comment: "Title for alert when email update fails"),
                            message: NSLocalizedString("err_account_update_failed", comment: "Message when update of account fails"),
                            preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("button_OK", comment: "Generic OK"), style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    };
                    self.onUpdateFailed()
                } else {
                    // Email updated...
                    self.updatePhase2()
                }
            })
        }
        else {
            updatePhase2()
        }
    }
    
    // Second update: password
    func updatePhase2() {
        // Update email
        if (newPassword.text != "") {
            let firebase = collectionView!.viewController!.firebaseRoot!
            firebase.changePasswordForUser(currentEmail(), fromOld: currentPassword.text, toNew: newPassword.text, withCompletionBlock: { error in
                if error != nil {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(
                            title: NSLocalizedString("title_err_password_update_failed", comment: "Title for alert when password update fails"),
                            message: NSLocalizedString("err_account_update_failed", comment: "Message when update of password fails"),
                            preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } ;
                    self.onUpdateFailed()
                } else {
                    // Email updated...
                    self.updatePhase3()
                }
            })
        }
        else {
            updatePhase3()
        }
    }
    
    // Third update: phone
    func updatePhase3() {
        if (phone.text != collectionView!.phone) {
            let firebase = collectionView!.viewController!.firebaseRoot!
            let uid = firebase.authData.uid
            let phoneStr = phone.text
            let phoneNumber = ["phone": phoneStr!]
        
            let thisUsersRef = Firebase(url: firebaseUrl + "/users/" + uid)
            thisUsersRef.setValue(phoneNumber)
            collectionView!.phone = phoneStr!
        }

        updateSuccess()
    }
    
    func updateSuccess() {
        var messageExtra = ""
        if (currentEmail() != emailAddress.text) {
            navigationController!.popViewControllerAnimated(true)
            messageExtra = ". " + NSLocalizedString("msg_updateSuccessful_login_again", comment: "After successful update, prompt the user to sign in again")
        }

        navigationController!.popViewControllerAnimated(true)
        
        if #available(iOS 8.0, *) {
            let alert = UIAlertController(
                title: NSLocalizedString("title_account_update_successful", comment: "Title for alert when account is successfully updated"),
                message: NSLocalizedString("msg_account_update_successful", comment: "Message when account update succeeds") + messageExtra,
                preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("button_OK", comment: ""), style: UIAlertActionStyle.Default, handler: nil))
            navigationController!.topViewController!.presentViewController(alert, animated: true, completion: nil)
        };
    }
    
    func onUpdateFailed() {
        currentPassword.text = ""
        newPassword.text = ""
        confirmNewPassword.text = ""
    }
    
    func validateInputs() -> Bool {
        var errorTitle : String? = nil
        var errorMessage : String? = nil
        let passwordProvided : Bool = currentPassword.text != ""

        if (emailAddress.text != currentEmail() && !passwordProvided) {
            errorTitle = NSLocalizedString("title_err_no_password", comment: "")
            errorMessage = NSLocalizedString("err_must_provide_current_password_to_change_email", comment: "Error message for when you haven't provided your current password to change email.")
        }
        else if (newPassword.text != "" && !passwordProvided) {
            errorTitle = NSLocalizedString("title_err_no_password", comment: "")
            errorMessage = NSLocalizedString("err_must_provide_current_password_to_change_it", comment: "You have to provide your current password to change it to a different password")
        }
        else if (newPassword.text != confirmNewPassword.text) {
            errorTitle = NSLocalizedString("title_err_mismatched_password", comment: "")
            errorMessage = NSLocalizedString("err_new_password_mismatch", comment: "When changing passwords, you must enter it twice the same way")
        }
        else if (newPassword.text != "" && newPassword.text!.utf16.count < 7) {
            errorTitle = NSLocalizedString("title_err_password_too_short", comment: "")
            errorMessage = NSLocalizedString("err_password_too_short", comment: "")
        }
        
        if (errorTitle != nil && errorMessage != nil) {
            if #available(iOS 8.0, *) {
               let alert = UIAlertController(
                    title: errorTitle!,
                    message: errorMessage!,
                    preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("button_OK", comment: ""), style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            };
            return false
        }
        
        return true
    }
}
