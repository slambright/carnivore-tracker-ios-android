//
//  SpeciesCollectionViewCell.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 3/1/15.
//  Copyright (c) 2015 Cheetah Conservation Fund. All rights reserved.
//

import UIKit

class SpeciesCollectionViewCell: UICollectionViewCell {
    let speciesNameLabel = UILabel()
    let speciesPreview = UIImageView()
    let imageDimensions = CGSize(width: 128, height: 96)
    var animalIndex = 0
    var xOffset : CGFloat = (150 - 128) / 2
    
    func setupSubViews() {
        self.clipsToBounds = false

        speciesNameLabel.text = "Test"
        //speciesNameLabel.textColor = UIColor.whiteColor()
        self.speciesPreview.frame = CGRect(x: xOffset, y: 10, width: imageDimensions.width, height:imageDimensions.height)
        
        // Width and height will be recalculated once we have actual text
        speciesNameLabel.frame = CGRect(x: xOffset, y: imageDimensions.height + 15,
            width: 10, height: 10)
        speciesNameLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        speciesNameLabel.numberOfLines = 2
        speciesNameLabel.clipsToBounds = false
        
        self.addSubview(speciesPreview)
        self.addSubview(speciesNameLabel)
    }
    
    func loadEditAccount() {
        loadData(NSLocalizedString("title_my_account", comment: "title for the link to get to My Account"), image: "gear")
    }
    
    func loadCredits() {
        loadData(NSLocalizedString("title_credits_about", comment: "title for the link to show credits"), image: "test")
    }
    
    func loadMySightings() {
        loadData(NSLocalizedString("title_my_sightings", comment: "Title for reported sightings"), image:"binoculars")
    }
    
    func loadData(labelText : NSString, image : NSString) {
        frame = CGRect(x: frame.origin.x, y: frame.origin.y,
            width: imageDimensions.width, height: imageDimensions.height)

        let imagePath = NSBundle.mainBundle().pathForResource((image as String) + "-128_96", ofType:"jpg")
        if (imagePath != nil) {
            self.speciesPreview.image = UIImage(contentsOfFile: imagePath!)
        }
        else {
            let imagePath2 = NSBundle.mainBundle().pathForResource((image as String) + "-128_96", ofType:"png")
            if (imagePath2 != nil) {
                self.speciesPreview.image = UIImage(contentsOfFile: imagePath2!)
            }
            else {
                self.speciesPreview.image = UIImage()
            }
        }
        
        speciesNameLabel.text = labelText as String;
        
        var speciesNameTextSize = labelText.sizeWithAttributes([NSFontAttributeName: speciesNameLabel.font])
        // Account for text wrapping
        speciesNameTextSize.width = speciesNameTextSize.width * 0.7
        speciesNameTextSize.width = max(imageDimensions.width, speciesNameTextSize.width)
        speciesNameTextSize.height = speciesNameTextSize.height * 2
        
        let minimumFrameSize = CGSize(
            width:max(frame.size.width, speciesNameTextSize.width + speciesNameLabel.frame.origin.x),
            height: max(frame.size.height, speciesNameTextSize.height + speciesNameLabel.frame.origin.y))
        frame = CGRect(x: frame.origin.x, y: frame.origin.y,
            width: minimumFrameSize.width, height: minimumFrameSize.height)
        speciesNameLabel.frame = CGRect(
            x: speciesNameLabel.frame.origin.x,
            y: speciesNameLabel.frame.origin.y,
            width: minimumFrameSize.width,
            height: speciesNameTextSize.height)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubViews()
    }
}
