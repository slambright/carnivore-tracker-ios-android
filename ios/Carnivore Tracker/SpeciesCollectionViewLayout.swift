//
//  Species.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 3/1/15.
//  Copyright (c) 2015 Cheetah Conservation Fund. All rights reserved.
//

import UIKit

class SpeciesCollectionViewLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        self.itemSize = CGSize(width: 150, height: 150)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.itemSize = CGSize(width: 150, height: 150)
    }

    //init(coder: )
    /*override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        var results = super.layoutAttributesForItemAtIndexPath(indexPath);
        results.size = CGSize(width: 2000, height: 1200)
        return results;
    }*/
}
