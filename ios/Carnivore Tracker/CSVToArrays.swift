//
//  CSVToArrays.swift
//  Carnivore Tracker
//
//  Created by Eric Berman on 10/15/15.
//  Copyright © 2015 Cheetah Conservation Fund. All rights reserved.
//

import Foundation

class CSVToArrays : NSObject, CHCSVParserDelegate {

    var parsedRows = [[String]]()

    func parseFromCSV(csvSource : String) {
        let csvp = CHCSVParser(CSVString: csvSource)
        csvp.delegate = self
        csvp.sanitizesFields = true;
        csvp.parse();
    }
    
    @objc func parser(parser : CHCSVParser!, didBeginLine recordNumber : UInt) {
        parsedRows.append([String]())
    }
    
    @objc func parser(parser : CHCSVParser!, didEndLine recordNumber : UInt) {
        
    }
    
    @objc func parser(parser : CHCSVParser!, didReadField field : String!, atIndex fieldIndex : Int) {
        parsedRows[parsedRows.count - 1].append(field)
    }
}