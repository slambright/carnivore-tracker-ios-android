//
//  MySightings.swift
//  Carnivore Tracker
//
//  Created by Eric Berman on 7/11/16.
//  Copyright © 2016 Cheetah Conservation Fund. All rights reserved.
//

import Foundation



class MySightings : UITableViewController {
    
    var sightings : [SightingRecord] = []
    var species : [SpeciesData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")

        let dataFile = AppDelegate.localSightingsFileName()
        let fileMgr: NSFileManager = NSFileManager.defaultManager()
        if fileMgr.fileExistsAtPath(dataFile) {
            let fileBuffer = fileMgr.contentsAtPath(dataFile)
            let csvData = NSString(data: fileBuffer!,
                                          encoding: NSUTF8StringEncoding)!

            let parser = CSVToArrays()
            parser.parseFromCSV(csvData as String)
            
            for row in parser.parsedRows {
                sightings.append(SightingRecord(timeStamp:dateFormatter.dateFromString(row[1]), species:row[2], count:row[3], notes:row[7]))
            }
        }
        
        species = SpeciesData.loadSpecies()
    }
    
    // MARK: - UITableView data source functions
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sightings.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("title_my_sightings", comment: "Title for reported sightings")
    }
    
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return sightings.count == 0 ? NSLocalizedString("err_no_sightings_found", comment: "Error for Sightings Found") : ""
    }
    
    func imageFromSpecies(speciesName:String?) -> String? {
        if (speciesName == nil) {
            return nil
        }
        
        for sp in species {
            if (sp.speciesName.compare(speciesName!) == NSComparisonResult.OrderedSame) {
                return sp.image as String
            }
        }
        return nil
    }
    
    func descriptionCell(tableView:UITableView, label: String, value: String, species: String?) -> UITableViewCell {
        let identifier = "sightingCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier)
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: identifier)
        }
        cell!.textLabel?.text = label
        cell!.textLabel?.font = UIFont.boldSystemFontOfSize(16.0)
        cell!.detailTextLabel!.text = value
        cell!.detailTextLabel!.font = UIFont.systemFontOfSize(14.0)
        cell!.detailTextLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        cell!.detailTextLabel?.numberOfLines = 2
        cell!.backgroundColor = UIColor.clearColor()
        cell!.backgroundView = UIView(frame: CGRect.zero)
        cell!.selectionStyle = UITableViewCellSelectionStyle.None
        cell!.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        cell!.imageView?.image = UIImage()
        if let imageName = imageFromSpecies(species) {
            let imagePath = NSBundle.mainBundle().pathForResource(imageName as String, ofType:"jpg") as String?
            if (imagePath != nil) {
                cell!.imageView?.image = UIImage(contentsOfFile: imagePath!)
        }
        }
        return cell!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let sighting = self.sightings[indexPath.row]

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle  = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")

        let l = NSString.init(format: "%@: (%@) %@", dateFormatter.stringFromDate(sighting.timeStamp!), sighting.count!, sighting.species!)
        
        let cell = self.descriptionCell(tableView, label: l as String, value: sighting.notes!, species:sighting.species)
        return cell
    }
}
