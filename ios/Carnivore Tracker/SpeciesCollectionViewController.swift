//
//  SpeciesCollectionViewController.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 3/1/15.
//  Copyright (c) 2016 Cheetah Conservation Fund. All rights reserved.
//

import CoreLocation
import UIKit

let reuseIdentifier = "SpeciesCell"

class SpeciesCollectionViewController: UICollectionViewController, UIAlertViewDelegate {
    var cells : [SpeciesData] = SpeciesData.loadSpecies()
    
    var viewController : ViewController? = nil
    var phone = ""
    var gpsCoord : CLLocation? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.registerClass(SpeciesCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:
            AWSRegionType.USEast1,
            identityPoolId: cognitoId)
        
        let defaultConfig = AWSServiceConfiguration(region: AWSRegionType.USWest1, credentialsProvider: credentialsProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = defaultConfig
        
        let firebase = viewController!.firebaseRoot
        if (firebase.authData != nil)
        {
            let thisUsersRef = Firebase(url: firebaseUrl + "/users/" + firebase.authData.uid)
        
            thisUsersRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
              let enumerator = snapshot.children
              while let rest = enumerator.nextObject() as? FDataSnapshot {
                    if (rest.key == "phone") {
                        self.phone = rest.value as! String
                        self.uploadSightings(false)
                    }
                }
            })
        }
    }
    
    func showAccount() -> Bool {
        return viewController!.firebaseRoot.authData != nil
    }
    
    func indexMyAccount() -> Int {
        return showAccount() ? self.cells.count : -1
    }
    
    func indexMySightings() -> Int {
        return self.cells.count + (showAccount() ? 1 : 0)
    }
    
    func indexCredits() -> Int {
        return indexMySightings() + 1
    }

    override func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let firebase = viewController?.firebaseRoot
        let cellIndex = indexPath.indexAtPosition(indexPath.length - 1)
        if (cellIndex < self.cells.count) {
            let speciesView = self.storyboard?.instantiateViewControllerWithIdentifier("speciesView") as! SpeciesViewController
            // The species view relies on our view's sighting upload capabilities
            speciesView.collectionView = self
            speciesView.canReportSighting = firebase != nil && firebase!.authData != nil
            speciesView.speciesData = self.cells[cellIndex]

            self.navigationController?.pushViewController(speciesView, animated: true)
        }
        else if (cellIndex == indexMyAccount()) {
            let editAccountView = self.storyboard?.instantiateViewControllerWithIdentifier("editAccountView") as! EditAccountViewController
            // The species view relies on our view's sighting upload capabilities
            editAccountView.collectionView = self
            self.navigationController?.pushViewController(editAccountView, animated: true)

            editAccountView.populate()
        }
        else if (cellIndex == indexMySightings()) {
            let sightingsView = self.storyboard?.instantiateViewControllerWithIdentifier("mySightingsView")
            self.navigationController?.pushViewController(sightingsView!, animated: true)
        }
        else if (cellIndex == indexCredits()) {
            let creditsView = self.storyboard?.instantiateViewControllerWithIdentifier("creditsView")
            // The species view relies on our view's sighting upload capabilities
            self.navigationController?.pushViewController(creditsView!, animated: true)
        }
        
        // Try to upload sightings whenever
        //   the user selects a species; in case
        //   we were offline and went online.
        if (self.phone != "") {
            self.uploadSightings(false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return indexCredits() + 1
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> SpeciesCollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! SpeciesCollectionViewCell

        // Configure the cell
        let cellIndex = indexPath.indexAtPosition(indexPath.length - 1)
        
        if (cellIndex < self.cells.count) {
            cell.animalIndex = cellIndex;
            self.cells[cell.animalIndex].loadCell(cell)
        }
        else if (cellIndex == indexMyAccount()) {
            cell.loadEditAccount()
        }
        else if (cellIndex == indexMySightings()) {
            cell.loadMySightings()
        }
        else if (cellIndex == indexCredits()) {
            cell.loadCredits()
        }
        
        let typedLayout = collectionView.collectionViewLayout as! SpeciesCollectionViewLayout
        typedLayout.itemSize.width = max(typedLayout.itemSize.width, cell.frame.size.width)
        typedLayout.itemSize.height = max(typedLayout.itemSize.height, cell.frame.size.height)
        
        return cell
    }
    
    func uploadSightings(newSightingReported : Bool) -> Bool {
        let dirPaths = NSSearchPathForDirectoriesInDomains(
            .DocumentDirectory, .UserDomainMask, true)
        
        let docsDir = dirPaths[0]
        let dataFile = (docsDir as NSString).stringByAppendingPathComponent("sightings.csv")
        
        var reportingsContents = NSString()
        let fileMgr = NSFileManager()
        if fileMgr.fileExistsAtPath(dataFile) {
            let fileBuffer = fileMgr.contentsAtPath(dataFile)
            reportingsContents = NSString(data: fileBuffer!,
                encoding: NSUTF8StringEncoding)!
            if (reportingsContents != "" && uploadSightingsStr(reportingsContents as String)) {
                do {
                    // Upload succeeded, clear the offline file
                    try "".writeToFile(dataFile, atomically: true,
                        encoding: NSUTF8StringEncoding)
                } catch _ {
                }
                
                // Tell the user
                let alert = UIAlertView(
                    title: NSLocalizedString("title_sightings_uploaded", comment: "Title for message box when sightings have been uploaded"),
                    message: NSLocalizedString("msg_sightings_report_confirmed", comment: "Message to confirm that sightings have been reported"),
                    delegate: self,
                    cancelButtonTitle: NSLocalizedString("button_Cancel", comment: "Generic Cancel button"),
                    otherButtonTitles: NSLocalizedString("button_OK", comment: "Generic OK button"))
                alert.show()
                return true
            }
            else if (reportingsContents == "") {
                return true
            }
        }
        
        if (newSightingReported) {
            let alert = UIAlertView(
                title: "Sighting Saved",
                message: NSLocalizedString("msg_sightings_report_offline", comment: "Message to display when sightings report submitted but offline"),
                delegate: self,
                cancelButtonTitle: NSLocalizedString("button_Cancel", comment: "Generic Cancel button"),
                otherButtonTitles: NSLocalizedString("button_OK", comment: "Generic OK button"))
            alert.show()
        }

        return false
    }

    func uploadSightingsStr(sightingsData : String) -> Bool {
        let firebase = viewController?.firebaseRoot
        if (viewController != nil && firebase != nil && firebase!.authData != nil) {
            let firebase = viewController!.firebaseRoot
            let email = firebase.authData.providerData["email"] as? String
            
            if (email != nil) {
                let emailSafe = email!.stringByReplacingOccurrencesOfString("@", withString: "_", options: [], range: nil)

                let date = NSDate()
                let dateFormatter = NSDateFormatter()
        
                dateFormatter.dateFormat = "AX"
        
                let uploadedName = emailSafe + "-" + dateFormatter.stringFromDate(date) + "-v1.csv"

                let dirPaths = NSSearchPathForDirectoriesInDomains(
                        .DocumentDirectory, .UserDomainMask, true)
        
                let docsDir = dirPaths[0]
                let dataFile = (docsDir as NSString).stringByAppendingPathComponent("upload-tmp.csv")
        
                let uploadContents = sightingsData.stringByReplacingOccurrencesOfString("<SOL>", withString: email! + "," + phone, options: [], range: nil)
                do {
                    try uploadContents.writeToFile(dataFile, atomically: true, encoding: NSUTF8StringEncoding)
                } catch _ {
                }

                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                transferManager.clearCache()
                let uploadReq = AWSS3TransferManagerUploadRequest()
                uploadReq.bucket = s3bucket
                uploadReq.key = uploadedName
                uploadReq.body = NSURL(fileURLWithPath: dataFile)
    
                let theUpload = transferManager.upload(uploadReq)
                theUpload.waitUntilFinished()
                print("Error:")
                print(theUpload.error == nil ? "" : theUpload.error)
                return theUpload.error == nil
            }
        }
        return false
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
