//
//  CreateAccountViewController.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 3/16/15.
//  Copyright (c) 2015 Cheetah Conservation Fund. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController, UIAlertViewDelegate {
    @IBOutlet weak var newAccountEmailField: UITextField!
    @IBOutlet weak var newAccountPasswordField: UITextField!
    @IBOutlet weak var newAccountPasswordConfirmField: UITextField!
    @IBOutlet weak var newAccountPhoneField: UITextField!
    
    var firebaseRoot : Firebase?
    var nativationController : UINavigationController?
    var loginViewController : ViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertView(
            title: title,
            message: message,
            delegate: self,
            cancelButtonTitle: NSLocalizedString("button_Cancel", comment: "Generic Cancel button"),
            otherButtonTitles: NSLocalizedString("button_OK", comment: "Generic OK button"))
        alert.show()
    }

    @IBAction func onCreateNewAccount(sender: AnyObject) {
        if (newAccountEmailField.text == "") {
            alert(NSLocalizedString("title_err_no_email", comment: "Title for missing email"),
                message: NSLocalizedString("err_no_email", comment: "Message for no email address provided"))
        }
        else if (newAccountPasswordField.text == "") {
            alert(NSLocalizedString("title_err_no_password", comment: "Title for the message saying that a password is required"),
                message: NSLocalizedString("err_no_password", comment: "Error message that a password is required"))
        }
        else if (newAccountPasswordField.text!.utf16.count < 7) {
            alert(NSLocalizedString("title_err_password_too_short", comment: "Title for error that password is too short."),
                message: NSLocalizedString("err_password_too_short", comment: "Error message when password is too short."))
        }
        else if (newAccountPasswordField.text !=
            newAccountPasswordConfirmField.text) {
            newAccountPasswordField.text = ""
                newAccountPasswordConfirmField.text = ""
                alert(NSLocalizedString("title_err_mismatched_password", comment: "Title for error when passwords don't match"),
                    message: NSLocalizedString("err_mismatched_password", comment: "Error when passwords do not match"))
        }
        else if (newAccountPhoneField.text == "") {
                alert(NSLocalizedString("title_err_no_phone", comment: "Title for error message where phone number is missing"),
                    message: NSLocalizedString("err_no_phone", comment: "Error message when no phone number provided"))
        }
        else {
            firebaseRoot?.createUser(newAccountEmailField.text, password: newAccountPasswordField.text, withValueCompletionBlock: { error, result in
                if (error != nil) {
                    self.alert(NSLocalizedString("title_err_account_create_failed", comment: "Title for error message when account creation fails"),
                        message: NSLocalizedString("err_account_create_failed", comment: "Error message when account creation fails"))
                }
                else {
                    self.onCreateSucceeded()
                }
            })
        }
    }

    func onCreateSucceeded() {
        alert(NSLocalizedString("title_err_account_creation_succcess", comment: "Title for message when account is created"),
            message: NSLocalizedString("msg_account_created", comment: "Message when an account is successfully created"))
        //self.presentViewController(alert, animated: true, completion: {
            self.loginViewController?.loginEmailField.text = self.newAccountEmailField.text
            self.navigationController?.popViewControllerAnimated(false)
        //})

        // Now that the user exists, log'em in so we can set the phone number
        self.firebaseRoot?.authUser(
            self.newAccountEmailField.text,
            password: self.newAccountPasswordField.text,withCompletionBlock: { error, authData in
            if error != nil {
                self.alert(NSLocalizedString("title_err_failed_logon", comment: "Title for error when account is created, but subsequent login fails"),
                    message: NSLocalizedString("err_account_succeeded_but_login_failed", comment: "Message when account is successfully created but login fails"))
            } else {
                // We are now logged in
                self.onLoginSucceeded()
            }
        })
    }

    func onLoginSucceeded() {
        // Now set the phone number
        let uid = firebaseRoot!.authData.uid
        let phone = self.newAccountPhoneField.text
        let phoneNumber = ["phone": phone!]

        let thisUsersRef = Firebase(url: firebaseUrl + "/users/" + uid)
        thisUsersRef.setValue(phoneNumber)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
