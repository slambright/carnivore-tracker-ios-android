//
//  ViewController.swift
//  Carnivore Tracker
//
//  Created by Steven Lambright on 2/22/15.
//  Copyright (c) 2015 Cheetah Conservation Fund. All rights reserved.
//

import UIKit

var firebaseUrl = "https://burning-torch-2674.firebaseio.com/"
var cognitoId = "us-west-2:dc571c26-550c-4e98-a964-49bd7ac91542"
var s3bucket = "ccftestdata"

class ViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {

    @IBOutlet weak var loginPasswordField: UITextField!

    @IBOutlet weak var loginEmailField: UITextField!
    var firebaseRoot = Firebase(url:firebaseUrl)
    var loggedIn = false
    var lastController : UIViewController? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController!.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        loginEmailField.delegate = self;
        loginPasswordField.delegate = self;
        
        firebaseRoot.observeAuthEventWithBlock({ authData in
            if authData != nil {
                // user authenticated with Firebase
                self.onLoginSucceeded()
            } else {
                // No user is logged in
                print("Not logged in")
            }
        })
    }
    
    @IBAction func onLoginSucceeded() {
        loggedIn = true
        loginPasswordField.text = ""
        let speciesTileView = self.storyboard?.instantiateViewControllerWithIdentifier("speciesCollectionView") as! SpeciesCollectionViewController
        speciesTileView.viewController = self
        self.navigationController?.pushViewController(speciesTileView, animated: true)
    }

    @IBAction func onLoginRequested(sender: AnyObject) {
        print("onLoginRequested - " + sender.description)
        firebaseRoot.authUser(loginEmailField.text, password: loginPasswordField.text, withCompletionBlock: { error, authData in
            if error != nil {
                self.loggedIn = false
                print("onLoginRequested... login failed")
                // There was an error logging in to this account
                
                let alert = UIAlertView(
                    title: NSLocalizedString("title_err_failed_logon", comment: "Title to display for alert when login fails"),
                    message: NSLocalizedString("err_LoginFailed", comment: "Message to display if login fails"),
                    delegate: self,
                    cancelButtonTitle: NSLocalizedString("button_Cancel", comment: "Generic Cancel button"),
                    otherButtonTitles: NSLocalizedString("button_OK", comment: "Generic OK button"))
                alert.show()
            } else {
                // onLoginSucceeded will be called automatically
            }})
    }
    
    @IBAction func onAnonymousViewSpeciesRequested(sender: AnyObject) {
        let speciesTileView = self.storyboard?.instantiateViewControllerWithIdentifier("speciesCollectionView") as! SpeciesCollectionViewController
        speciesTileView.viewController = self
        self.navigationController?.pushViewController(speciesTileView, animated: true)
    }
    
    @IBAction func onEmailAddressEditingFinished(sender: UITextField) {
        print("onEmailAddressEditingFinished")
        loginPasswordField.becomeFirstResponder()
    }

    @IBAction func onForgotPasswordRequested(sender: AnyObject) {
        firebaseRoot.resetPasswordForUser(loginEmailField.text, withCompletionBlock: { error in
            if (error != nil) {
                let alert = UIAlertView(
                    title: NSLocalizedString("title_Password_Reset", comment: "Title to display on alert to reset password"),
                    message: NSLocalizedString("msg_password_reset_failed", comment: "Message to display when password reset fails"),
                    delegate: self,
                    cancelButtonTitle: NSLocalizedString("button_Cancel", comment: "Generic Cancel button"),
                    otherButtonTitles: NSLocalizedString("button_OK", comment: "Generic OK button"))
                alert.show()
            }
            else {
                let alert = UIAlertView(
                    title: NSLocalizedString("title_Password_Reset", comment: "Title to display on alert to reset password"),
                    message: NSLocalizedString("msg_password_reset_mail_sent", comment: "Tells the user to whom the reset email was sent.") +
                        self.loginEmailField.text!,
                    delegate: self,
                    cancelButtonTitle: NSLocalizedString("button_Cancel", comment: "Generic Cancel button"),
                    otherButtonTitles: NSLocalizedString("button_OK", comment: "Generic OK button"))
                alert.show()
            }})
    }

    @IBAction func onCreateAccountRequested(sender: AnyObject) {
        let createAccountView = self.storyboard?.instantiateViewControllerWithIdentifier("createAccountView") as! CreateAccountViewController
        createAccountView.firebaseRoot = firebaseRoot
        createAccountView.nativationController = navigationController
        createAccountView.loginViewController = self
        print(createAccountView)
        self.navigationController?.pushViewController(createAccountView, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ViewController : UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController,
        willShowViewController viewController: UIViewController,
        animated: Bool) {
        if (lastController != nil &&
            lastController is SpeciesCollectionViewController &&
            viewController is ViewController) {
                // We're navigating from the collection view to the login view, log out.
                confirmLogout()
        }
        lastController = viewController
        // We'll populate these once the graphic is over
        navigationController.navigationBar.topItem?.title = "";
        navigationController.navigationBar.backItem?.title = "";
    }
    
    func confirmLogout() {
        if (self.firebaseRoot.authData == nil)
        {
            return
        }
        
        if #available(iOS 8.0, *) {
            let alert = UIAlertController(
                title: NSLocalizedString("title_logout", comment: ""),
                message: NSLocalizedString("msg_confirm_logout", comment: ""),
                preferredStyle: UIAlertControllerStyle.Alert)
            
                let logoutHandler = { (action:UIAlertAction!) -> Void in
                    self.firebaseRoot.unauth();
                }
                let cancelHandler = { (action:UIAlertAction!) -> Void in
                    self.onLoginSucceeded()
                }
                let completionHandler = { () -> Void in
                    if (self.loggedIn) {
                        self.onLoginSucceeded()
                    }
                }
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("button_Yes", comment: ""), style: UIAlertActionStyle.Default, handler: logoutHandler))
            alert.addAction(UIAlertAction(title: NSLocalizedString("button_Cancel", comment: ""), style: UIAlertActionStyle.Default, handler: cancelHandler))
            self.presentViewController(alert, animated: true, completion: completionHandler)
        }
        else {
            firebaseRoot.unauth()
        }
    }

    func navigationController(navigationController: UINavigationController,
        didShowViewController viewController: UIViewController,
        animated: Bool) {
        if (viewController is SpeciesCollectionViewController &&
            self.firebaseRoot.authData != nil) {
            navigationController.navigationBar.backItem?.title = NSLocalizedString("title_logout", comment: "Title for logout screen");
            navigationController.navigationBar.topItem?.title = NSLocalizedString("title_spot_report", comment: "\"Spot it, Report it\" slogan used throughout")
        }
        else {
            navigationController.navigationBar.backItem?.title = NSLocalizedString("title_back", comment: "Generic Back button title");
            navigationController.navigationBar.topItem?.title = ""
        }
    }
}

