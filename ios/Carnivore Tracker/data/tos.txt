Carnivore Tracker
Copyright (c) 2015 Cheetah Conservation Fund

*** END USER LICENSE AGREEMENT ***

IMPORTANT: PLEASE READ THIS LICENSE CAREFULLY BEFORE USING THIS SOFTWARE.

1. LICENSE

By receiving, opening the file package, and/or using Carnivore Tracker ("Software") containing this software, you agree that this End User User License Agreement(EULA) is a legally binding and valid contract and agree to be bound by it. You agree to abide by the intellectual property laws and all of the terms and conditions of this Agreement.

Unless you have a different license agreement signed by Cheetah Conservation Fund your use of Carnivore Tracker indicates your acceptance of this license agreement and warranty.

Subject to the terms of this Agreement, Cheetah Conservation Fund grants to you a limited, non-exclusive, non-transferable license, without right to sub-license, to use Carnivore Tracker in accordance with this Agreement and any other written agreement with Cheetah Conservation Fund. Cheetah Conservation Fund does not transfer the title of Carnivore Tracker to you; the license granted to you is not a sale. This agreement is a binding legal agreement between Cheetah Conservation Fund and the users of Carnivore Tracker.

If you do not agree to be bound by this agreement, remove Carnivore Tracker from your computer or phone now.

2. DISTRIBUTION

Carnivore Tracker and the license herein granted shall not be copied, shared, distributed, re-sold, offered for re-sale, transferred or sub-licensed in whole or in part except that you may make one copy for archive purposes only. For information about redistribution of Carnivore Tracker contact Cheetah Conservation Fund.

3. USER AGREEMENT

3.1 Use Restrictions

You shall use Carnivore Tracker in compliance with all applicable laws and not for any unlawful purpose. Without limiting the foregoing, use, display or distribution of Carnivore Tracker together with material that is pornographic, racist, vulgar, obscene, defamatory, libelous, abusive, promoting hatred, discriminating or displaying prejudice based on religion, ethnic heritage, race, sexual orientation or age is strictly prohibited.

3.2 Copyright Restriction

This Software contains copyrighted material, trade secrets and other proprietary material. You shall not, and shall not attempt to, modify, reverse engineer, disassemble or decompile Carnivore Tracker. Nor can you create any derivative works or other works that are based upon or derived from Carnivore Tracker in whole or in part.

Cheetah Conservation Fund's name, logo and graphics file that represents Carnivore Tracker shall not be used in any way to promote products developed with Carnivore Tracker. Cheetah Conservation Fund retains sole and exclusive ownership of all right, title and interest in and to Carnivore Tracker and all Intellectual Property rights relating thereto.

Copyright law and international copyright treaty provisions protect all parts of Carnivore Tracker, products and services. No program, code, part, image, audio sample, or text may be copied or used in any way by the user except as intended within the bounds of the single user program. All rights not expressly granted hereunder are reserved for Cheetah Conservation Fund.

3.3 Limitation of Responsibility

You will indemnify, hold harmless, and defend Cheetah Conservation Fund, its employees, agents and distributors against any and all claims, proceedings, demand and costs resulting from or in any way connected with your use of Cheetah Conservation Fund's Software.

In no event (including, without limitation, in the event of negligence) will Cheetah Conservation Fund, its employees, agents or distributors be liable for any consequential, incidental, indirect, special or punitive damages whatsoever (including, without limitation, damages for loss of profits, loss of use, business interruption, loss of information or data, or pecuniary loss), in connection with or arising out of or related to this Agreement, Carnivore Tracker or the use or inability to use Carnivore Tracker or the furnishing, performance or use of any other matters hereunder whether based upon contract, tort or any other theory including negligence.

Cheetah Conservation Fund's entire liability, without exception, is limited to the customers' reimbursement of the purchase price of the Software (maximum being the lesser of the amount paid by you and the suggested retail price as listed by Cheetah Conservation Fund) in exchange for the return of the product, all copies, registration papers and manuals, and all materials that constitute a transfer of license from the customer back to Cheetah Conservation Fund.

3.4 Warranties

Except as expressly stated in writing, Cheetah Conservation Fund makes no representation or warranties in respect of this Software and expressly excludes all other warranties, expressed or implied, oral or written, including, without limitation, any implied warranties of merchantable quality or fitness for a particular purpose.

3.5 Termination

Any failure to comply with the terms and conditions of this Agreement will result in automatic and immediate termination of this license. Upon termination of this license granted herein for any reason, you agree to immediately cease use of Carnivore Tracker and destroy all copies of Carnivore Tracker supplied under this Agreement. The financial obligations incurred by you shall survive the expiration or termination of this license.

4. DISCLAIMER OF WARRANTY

THIS SOFTWARE AND THE ACCOMPANYING FILES ARE SOLD "AS IS" AND WITHOUT WARRANTIES AS TO PERFORMANCE OR MERCHANTABILITY OR ANY OTHER WARRANTIES WHETHER EXPRESSED OR IMPLIED. THIS DISCLAIMER CONCERNS ALL FILES GENERATED AND EDITED BY Carnivore Tracker AS WELL.

5. CONSENT OF USE OF DATA

You agree that Cheetah Conservation Fund may collect and use information gathered in any manner as part of the product support services provided to you, if any, related to Carnivore Tracker. Cheetah Conservation Fund may also use this information to provide notices to you which may be of use or interest to you.
 	