/* 
  Localizable.strings
  Carnivore Tracker

  Created by Eric Berman on 10/13/15.
  Copyright © 2015 Cheetah Conservation Fund. All rights reserved.
*/

/* Title to display for alert when login fails */
"title_err_failed_logon" = "Login Failed";
/* Generic "Done" button */
"button_done" = "Done";
/* Placeholder to prompt a user to make a choice */
"placeholder_downpicker_noselection" = "Tap to choose...";
/* Placeholder to prompt a user while selecting */
"placeholder_downpicker_selection" = "Pick an option...";
/* Message to display if login fails */
"err_LoginFailed" = "If you don't have an account, please create one first. Otherwise, please check your username and password. If you forgot your password, enter your email and choose \"Forgot Password.\"";
/* Generic Cancel button */
"button_Cancel" = "Cancel";
/* Generic OK button */
"button_OK" = "OK";
/* Generic Yes button */
"button_Yes" = "Yes";
/* Title to display on alert to reset password */
"title_Password_Reset" = "Password Reset";
/* Message to display when password reset fails */
"msg_password_reset_failed" = "Password reset failed. Please make sure you entered your email correctly, or create an account if you haven't registered.";
/* Tells the user to whom the reset email was sent. */
"msg_password_reset_mail_sent" = "Password reset email sent to: ";
/* Title for logout screen */
"title_logout" = "Logout";
/* Message for confirm logout */
"msg_confirm_logout" = "Are you sure you want to log out? Logging in to report sightings requires internet access.";
/* "Spot it, Report it" slogan used throughout */
"title_spot_report" = "Spot it, Report it";
/* Generic Back button title */
"title_back" = "Back";
/* Title for message box when sightings have been uploaded */
"title_sightings_uploaded" = "Sightings Uploaded";
/* Message to confirm that sightings have been reported */
"msg_sightings_report_confirmed" = "Your sightings have been reported, thank you!";
/* Message to display when sightings report submitted but offline */
"msg_sightings_report_offline" = "Your sightings have been saved and will be uploaded as soon as possible, thank you!";
/* title for the link to show credits */
"title_credits_about" = "Credits / About Us";
/* title for the link to get to My Account */
"title_my_account" = "My Account";
/* Label for the name of the species */
"label_species_name" = "Species Name:";
/* Label for alternative names for the species */
"label_species_other_names" = "Other Names:";
/* Label for the IUCN status for the species */
"label_iucn_status" = "IUCN Status:";
/* Label for the habitat of the species */
"label_species_habitat" = "Habitat:";
/* Label for the diet of the species */
"label_species_diet" = "Diet:";
/* Label for the active time of the species */
"label_activity" = "Active During:";
/* Label for the size of the species */
"label_size" = "Size:";
/* Label for the weight of the species */
"label_weight" = "Weight:";
/* Label for the social behavior of the species */
"label_social_behavior" = "Social Behavior:";
/* Label for the sightings map of the species */
"label_map" = "Reported Sightings Map";
/* Label for the place to enter the number of individuals that were spotted */
"label_number_of_individuals" = "Number of Individuals:";
/* Label for free-form notes */
"label_free_text" = "Add any additional information:";
/* Button to report a sighting */
"button_report_sighting" = "Report Sighting";
/* Button to show a map of reported sightings */
"button_open_map" = "Show Reported Sightings Map";
/* Title of error when no GPS coordinate is available */
"title_err_no_gps" = "GPS Location Not Determined";
/* Error message when no GPS coordinate is available */
"err_no_gps" = "Your GPS location is required to report sightings, but your device's GPS hasn't provided a location yet. Please try again in a minute.";
/* Title for missing email */
"title_err_no_email" = "Email Required";
/* Message for no email address provided */
"err_no_email" = "Please enter your email address. All fields are required.";
/* Title for the message saying that a password is required */
"title_err_no_password" = "Password Required";
/* Error message that a password is required */
"err_no_password" = "Please enter a password. All fields are required.";
/* Title for error that password is too short. */
"title_err_password_too_short" = "Password Too Short";
/* Error message when password is too short. */
"err_password_too_short" = "Your password is too short. Please choose a longer password.";
/* Title for error when passwords don't match */
"title_err_mismatched_password" = "Passwords Did Not Match";
/* Error when passwords do not match */
"err_mismatched_password" = "Your passwords did not match. Please re-enter your password.";
/* Title for error message where phone number is missing */
"title_err_no_phone" = "Phone Number Missing";
/* Error message when no phone number provided */
"err_no_phone" = "Please enter your phone number; this information is necessary to confirm species sightings.";
/* Title for error message when account creation fails */
"title_err_account_create_failed" = "Failed to Create Account";
/* Error message when account creation fails */
"err_account_create_failed" = "Account creation failed. Please make sure you are online and entered a valid email address.";
/* Title for message when account is created */
"title_err_account_creation_succcess" = "Account Created";
/* Message when an account is successfully created */
"msg_account_created" = "Your account was successfully created";
/* Message when account is successfully created but login fails */
"err_account_succeeded_but_login_failed" = "Account created, but login failed. Please check your internet connection.";
/* Title for alert when email update fails */
"title_err_email_update_failed" = "Email Update Failed";
/* Message when update of account fails */
"err_account_update_failed" = "Failed to update your account details, please make sure you're online and entered the correct password.";
/* Title for alert when password update fails */
"title_err_password_update_failed" = "Password Update Failed";
/* After successful update, prompt the user to sign in again */
"msg_updateSuccessful_login_again" = "Please log in with your new email address.";
/* Title for alert when account is successfully updated */
"title_account_update_successful" = "Account Updated";
/* Message when account update succeeds */
"msg_account_update_successful" = "Your account details have been updated";
/* Error message for when you haven't provided your current password to change email. */
"err_must_provide_current_password_to_change_email" = "Please enter your password in order to change your email address";
/* You have to provide your current password to change it to a different password */
"err_must_provide_current_password_to_change_it" = "Please enter your password in order to change your password";
/* When changing passwords, you must enter it twice the same way */
"err_new_password_mismatch" = "The new passwords you entered did not match";
/* The name of the CSV file that has the species data in it. */
"file_species_data" = "species-en";
/* Title for reported sightings */
"title_my_sightings" = "My Sightings";
/* Error for Sightings Found */
"err_no_sightings_found" = "No Sightings Found";