# Carnivore Tracker - iOS app

## Building with XCode 7.3

You'll need Mac OS X El Capitan and XCode 7.3 to build this app. Then you'll
need to run the following from a terminal in the root directory of the repo:

```bash
$ sudo gem install cocoapods
$ pod install
```

Once all the app's dependencies have been installed, you should be able to load
the `.xcodeproj` file and build the app.
