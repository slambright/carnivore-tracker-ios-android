#!/bin/bash

IN="$1"
REF="$2"
OUT="$3"
COLOR="$4"

echo "$1 -> $2 -> $3"

DIMS=`file "$2" | sed 's#.* \([0-9]* x [0-9]*\).*#\1#' | tr -d ' '`

DIM1=`echo $DIMS | sed 's/x.*//'`
DIM2=`echo $DIMS | sed 's/.*x//'`

echo "Reference image is $DIMS"

convert "$IN" -resize "${DIMS}" -background "$4" -compose Copy -gravity center -extent "$DIMS" "$OUT"

