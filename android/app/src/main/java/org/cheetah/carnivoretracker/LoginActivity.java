package org.cheetah.carnivoretracker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private boolean mLicenseAgreedTo;

    private Firebase firebase;

    private static final int SELECTION_ACTIVITY_RESULT = 1;
    private static final int REGISTER_ACTIVITY_RESULT = 2;

    private static final String license_agreed_touchfile = "license_agreed_v1.0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLicenseAgreedTo = false;

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        firebase = getApp().firebase;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkLicenseAgreed();
                maybeLoginComplete();
            }
        }, 1000);
    }

    private void getPermissions() {

    }

    private void checkLicenseAgreed() {

        try {
            openFileInput(license_agreed_touchfile);
            mLicenseAgreedTo = true;
            return;
        }
        catch (java.io.FileNotFoundException e) {
            // License hasn't been agreed to... we need to prompt it
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getText(R.string.license_agreement)).setTitle(getString(R.string.title_license_agreement));
        builder.setPositiveButton(getString(R.string.action_agree_terms), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                userAgreedToLicense();
            }
        });
        builder.setNegativeButton(getString(R.string.action_decline_terms), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                userDisagreedWithLicense();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                checkLicenseAgreed();
            }
        });
        dialog.show();
    }

    private void userAgreedToLicense() {
        // User agreed
        try {
            openFileOutput(license_agreed_touchfile, MODE_PRIVATE);
        }
        catch (java.io.FileNotFoundException ex) {
            // Shouldn't be possible
        }
        mLicenseAgreedTo = true;
        maybeLoginComplete();
    }

    private void userDisagreedWithLicense() {
        finish();
    }

    void maybeLoginComplete()
    {
        if (!mLicenseAgreedTo) return;
        showProgress(false);
        if (firebase.getAuth() == null) return;
        onLoginComplete();
    }

    private void onLoginComplete() {
        Intent a = new Intent(this,SpeciesSelectionActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(a, SELECTION_ACTIVITY_RESULT);
    }

    private void showCreateUserActivity()
    {
        if (!mLicenseAgreedTo) return;
        Intent a = new Intent(this,CreateAccountActivity.class);
        a.putExtra("email", mEmailView.getText().toString());
        startActivityForResult(a, REGISTER_ACTIVITY_RESULT);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (!mLicenseAgreedTo) return;

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        getApp().mEmail = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(getApp().mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            showProgress(false);
            focusView.requestFocus();
        } else {

            if (TextUtils.isEmpty(password)) {
                showCreateUserActivity();
            }
            else {
                tryUserLogin();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about_us) {
            getApp().showAboutUs(this);
            return true;
        }
        else if (id == R.id.reset_password) {
            tryResetPassword();
        }

        return super.onOptionsItemSelected(item);
    }

    private void tryUserLogin() {
        showProgress(true);
        String password = mPasswordView.getText().toString();
        mPasswordView.setText("");
        firebase.unauth();
        firebase.authWithPassword(getApp().mEmail, password, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                maybeLoginComplete();
            }

            @Override
            public void onAuthenticationError(FirebaseError error) {
                showProgress(false);
                if (error.getCode() == FirebaseError.INVALID_EMAIL) {
                    mEmailView.setError(getString(R.string.error_invalid_email));
                    mEmailView.requestFocus();
                } else {
                    mPasswordView.setError(getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                }
            }
        });
    }

    private void tryResetPassword() {
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        if (email.isEmpty()) {
            mEmailView.setError(getString(R.string.error_email_required));
            mEmailView.requestFocus();
            return;
        }

        showProgress(true);
        getApp().firebase.resetPassword(email, new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                showProgress(false);
                showPasswordResetComplete();
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                showProgress(false);
                if (firebaseError.getCode() == FirebaseError.NETWORK_ERROR) {
                    showPasswordResetFailed();
                } else {
                    showPasswordResetComplete();
                }
            }
        });
    }

    private void showPasswordResetComplete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.status_password_reset_sent)).setTitle(getString(R.string.title_reset_password));
        builder.setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showPasswordResetFailed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.status_password_reset_failed)).setTitle(getString(R.string.title_reset_password));
        builder.setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static boolean isPasswordValid(String password) {
        return password.length() > 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // Try to switch to the species selection view... if we logged out, then this will
        //   silently fail. If we created an account, then this will just switch our view.

        if (requestCode == SELECTION_ACTIVITY_RESULT) {
            if (getApp().mQuitApp) {
                // So we don't preserve this flag when the user launches the app again
                getApp().mQuitApp = false;
                finish();
            }
        }
        else {
            maybeLoginComplete();
        }
    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    public CarnivoreTrackerApplication getApp()
    {
        return (CarnivoreTrackerApplication )super.getApplication();
    }
}



