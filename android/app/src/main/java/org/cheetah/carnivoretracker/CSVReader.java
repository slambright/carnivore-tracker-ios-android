package org.cheetah.carnivoretracker;

import java.util.ArrayList;

/**
 * Created by ericberman on 10/14/15.
 * Adapted from code from MyFlightbook which was adapted from JouniHeikniemi.Tools.Text
 */
public class CSVReader {

    private String ListSeparator;

    private String BestGuessSeparator(String szLine) {
        String szSep = ListSeparator;

        int cElements = -1;

        ArrayList<String> al = new ArrayList<>();

        String szSepSave = ListSeparator;

        final String KnownCSVSeparators = ", ; \t"; // space separates them

        // try each possible separator on the row, see which one yields the most columns - that's our best guess.
        String[] rgSeps = KnownCSVSeparators.split(" ");
        for (String sep : rgSeps) {
            ListSeparator = sep;
            ParseCSVFields(al, szLine);
            if (al.size() > cElements) {
                szSep = sep;
                cElements = al.size();
            }
            al.clear();
        }

        // Restore the prior list separator
        ListSeparator = szSepSave;

        return szSep;
    }

    public CSVReader() {
    }

    private class ParseFieldResult {
        public String result;
        public int position;

        public ParseFieldResult() {
            result = "";
            position = -1;
        }
    }

    /// <summary>
    /// Returns the fields for the next row of CSV data (or null if at eof)
    /// </summary>
    /// <param name="fInferSeparator">True to examine the first line to determine most likely separator</param>
    /// <returns>A String array of fields or null if at the end of file.</returns>
    public String[] GetCSVLine(String data, Boolean fInferSeparator) {
        if (data == null) return null;
        if (data.length() == 0) return new String[0];

        if (fInferSeparator)
            ListSeparator = BestGuessSeparator(data);

        ArrayList<String> result = new ArrayList<>();

        ParseCSVFields(result, data);

        return result.toArray(new String[result.size()]);
    }

    // Parses the CSV fields and pushes the fields into the result arraylist
    private void ParseCSVFields(ArrayList<String> result, String data) {
        ParseFieldResult pfr = new ParseFieldResult();
        while (pfr.position < data.length()) {
            ParseCSVField(data, pfr);
            result.add(pfr.result);
        }
    }

    // Parses the field at the given position of the data, modified pos to match
    // the first unparsed position and returns the parsed field
    private void ParseCSVField(String data, ParseFieldResult pfr) {

        if (pfr.position == data.length() - 1) {
            pfr.position++;
            // The last field is empty
            pfr.result = "";
            return;
        }

        int fromPos = pfr.position + 1;

        // Determine if this is a quoted field
        if (data.charAt(fromPos) == '"') {
            // If we're at the end of the String, let's consider this a field that
            // only contains the quote
            if (fromPos == data.length() - 1) {
                pfr.position++;
                pfr.result = "\"";
                return;
            }

            // Otherwise, return a String of appropriate length with double quotes collapsed
            // Note that FSQ returns data.length() if no single quote was found
            int nextSingleQuote = FindSingleQuote(data, fromPos + 1);
            pfr.position = nextSingleQuote + 1;
            pfr.result = data.substring(fromPos + 1, nextSingleQuote).replace("\"\"", "\"");
            return;
        }

        // The field ends in the next comma or EOL
        int nextComma = data.indexOf(ListSeparator, fromPos);
        if (nextComma == -1) {
            pfr.position = data.length();
            pfr.result = data.substring(fromPos);
        } else {
            pfr.position = nextComma;
            pfr.result = data.substring(fromPos, nextComma);
        }
    }

    // Returns the index of the next single quote mark in the String
    // (starting from startFrom)
    private int FindSingleQuote(String data, int startFrom) {

        int i = startFrom - 1;
        while (++i < data.length())
            if (data.charAt(i) == '"') {
                // If this is a double quote, bypass the chars
                if (i < data.length() - 1 && data.charAt(i + 1) == '"') {
                    i++;
                } else
                    return i;
            }
        // If no quote found, return the end value of i (data.length())
        return i;
    }
}
