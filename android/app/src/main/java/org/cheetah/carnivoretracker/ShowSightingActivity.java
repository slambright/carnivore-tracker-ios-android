package org.cheetah.carnivoretracker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;
import java.util.Formatter;
import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ShowSightingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_sighting);

        String localFile = getApp().localSaveFilename();
        String content = getSightingList(localFile);
        String sightings = processSightingList(content);

        TextView textView = (TextView) findViewById(R.id.show_sighting_text);
        ListView listView = (ListView) findViewById(R.id.show_sightings_list);

        ArrayList<String> sightingsList = new ArrayList<String>();
        sightingsList.addAll( Arrays.asList(sightings.split("\n")) );

        // Create ArrayAdapter using the planet list.
        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.show_sighting_list_item, sightingsList);

        if (sightings.isEmpty()) {
            textView.setText(getString(R.string.show_sighting_empty_text));
        }
        else {
            textView.setText("");
        }
        listView.setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_sighting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private String getSightingList(String localFile) {
        String fileContents = "";
        try {

            FileInputStream inStream = getApp().openFileInput(localFile);
            byte[] buffer = new byte[1025];
            int readBytes;
            do {
                buffer[0] = '\0';
                readBytes = inStream.read(buffer, 0, 1024);
                if (readBytes > 0) {
                    buffer[readBytes] = '\0';
                    fileContents = fileContents + new String(buffer, 0, readBytes);
                }
            } while (readBytes != -1);

            inStream.close();
        } catch (FileNotFoundException ignored) {
            Log.w("onCreate-List Sightings", "File not found");
        } catch (IOException ex) {
            String str = ex.getMessage();
            Log.e("onCreate-List Sightings", "Exception: " + str);
        }

        return fileContents.trim();
    }

    private String processSightingList(String content) {
        String[] lines = content.split("\n");
        StringBuilder results = new StringBuilder();
        int count = 1;
        for (String line : lines) {
            if (line.isEmpty())
                continue;

            String[] cols = line.split(",");
            // assume fixed format
            if (cols.length < 4)
                continue;

            results.append(count);
            count++;
            results.append(") ");
            // date time
            results.append(GetLocalTime(cols[1]));
            results.append(": ");
            // name
            results.append(cols[2]);
            // count
            if (!cols[3].equals("1")) {
                results.append(" (" + cols[3] + ")");
            }

            results.append("\n");
        }
        return results.toString();
    }

    private String GetLocalTime(String utcTime) {
        // Date time - yyyy-MM-dd'T'HH:mm:ssZ (UTC)
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            Date dt = sdf.parse(utcTime);

            sdf = new SimpleDateFormat();
            // local time. could be overridden?
            sdf.setTimeZone(TimeZone.getDefault());

            return sdf.format(dt);
        }
        catch (ParseException e)
        {
            return utcTime;
        }

    }

    public CarnivoreTrackerApplication getApp() {
        return (CarnivoreTrackerApplication) super.getApplication();
    }

}
