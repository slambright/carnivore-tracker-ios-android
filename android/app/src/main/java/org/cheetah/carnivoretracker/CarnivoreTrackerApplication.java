package org.cheetah.carnivoretracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.regions.Regions;
import com.firebase.client.AuthData;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;

public class CarnivoreTrackerApplication extends MultiDexApplication {
    static final String firebaseUrl = "https://burning-torch-2674.firebaseio.com";
    public Firebase firebase = null;
    public String mPhone = "";
    public String mEmail = "";
    public boolean mQuitApp = false;

    @Override
    public void onCreate()
    {
        super.onCreate();

        initSingletons();
    }

    protected void initSingletons() {
        Firebase.setAndroidContext(this);
        firebase = new Firebase(firebaseUrl);

        firebase.addAuthStateListener(new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                addPhoneNumberListener();
            }
        });

        Species.initSpecies(this.getApplicationContext());
    }

    private void addPhoneNumberListener() {
        if (firebase.getAuth() == null) return;

        mEmail = (String)firebase.getAuth().getProviderData().get("email");

        firebase.child("users").child(firebase.getAuth().getUid()).addChildEventListener(new ChildEventListener() {
            private void readSnapshot(DataSnapshot snapshot) {
                if (firebase.getAuth() == null) return;

                if (snapshot != null) {
                    mPhone = (String) snapshot.getValue();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot snapshot, String str) {
                readSnapshot(snapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot snapshot, String str) {
                readSnapshot(snapshot);
            }

            @Override
            public void onChildAdded(DataSnapshot snapshot, String str) {
                readSnapshot(snapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot snapshot) {
                readSnapshot(snapshot);
            }

            @Override
            public void onCancelled(FirebaseError error) {
                logout();
            }
        });
    }

    public void logout()
    {
        mPhone = "";
        firebase.unauth();
    }

    public boolean uploadSightings(final Activity currentActivity)
    {
        String fileData = getOfflineSightingsData(currentActivity);

        if (fileData.isEmpty()) return false;
        if (mEmail.isEmpty()) return false;
        if (mPhone.isEmpty()) return false;

        fileData = fileData.replaceAll("<SOL>,", mEmail + "," + mPhone + ",");

        CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                currentActivity,    // get the context for the current activity
                "us-west-2:dc571c26-550c-4e98-a964-49bd7ac91542",    /* Identity Pool ID */
                Regions.US_WEST_2           /* Region */
        );

        String toUploadFileName = offlineSaveFilename() + ".toupload";
        try {
            FileOutputStream outStream = currentActivity.openFileOutput(toUploadFileName, 0);
            outStream.write(fileData.getBytes());
            outStream.close();
        }
        catch (java.io.IOException ex) {
            return false;
        }

        File toUpload = new File(currentActivity.getFilesDir() + "/" + toUploadFileName);

        String uploadedName = mEmail.replace("@", "_") + "-" + (new Date().getTime()) + "-v1.csv";

        TransferManager transferManager = new TransferManager(cognitoProvider);
        Upload upload = transferManager.upload("ccftestdata", uploadedName, toUpload);

        try {
            upload.waitForCompletion();
            setOfflineSightingData(currentActivity, "");
            return true;
        }
        catch (java.lang.Exception ignored) {

        }

        return false;
    }

    public void showUploadSuccessDialog(final Activity currentActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(currentActivity);
        builder.setMessage(getString(R.string.action_upload_sightings)).setTitle(getString(R.string.action_upload_sightings_title));
        builder.setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public boolean reportSighting(final Activity currentActivity, String sightingsLine) {
        String sightings = sightingsLine + "\n";

        String localFile = localSaveFilename();
        try {
            FileOutputStream outStream = currentActivity.openFileOutput(localFile, MODE_APPEND);
            outStream.write(sightings.getBytes());
            outStream.close();
        }
        catch (java.io.IOException ex) {
        }

        sightings += getOfflineSightingsData(currentActivity);
        setOfflineSightingData(currentActivity, sightings);

        return uploadSightings(currentActivity);
    }

    private void setOfflineSightingData(final Activity currentActivity, String fileData) {
        String filename = offlineSaveFilename();
        try {
            FileOutputStream outStream = currentActivity.openFileOutput(filename, 0);
            outStream.write(fileData.getBytes());
            outStream.close();
        }
        catch (java.io.IOException ex) {
            Log.w("setOfflineSightingData", "Failed to write sightings data to file: " + ex.toString());
        }
    }

    private String getOfflineSightingsData(final Activity currentActivity) {
        String filename = offlineSaveFilename();
        String fileContents = "";
        try {
            FileInputStream inStream = currentActivity.openFileInput(filename);

            byte[] buffer = new byte[1025];
            int readBytes;
            do {
                buffer[0] = '\0';
                readBytes = inStream.read(buffer, 0, 1024);
                if (readBytes > 0) {
                    buffer[readBytes] = '\0';
                    fileContents = fileContents + new String(buffer, 0, readBytes);
                }
            } while (readBytes != -1);
        } catch (FileNotFoundException ignored) {
            Log.w("getOfflineSightingsDate", "File not found");
        } catch (IOException ex) {
            Log.e("getOfflineSightingsData", "Exception: " + ex.getMessage());
        }

        return fileContents;
    }

    private String offlineSaveFilename()
    {
        String filename = "upload_tmp.csv";
        if (firebase.getAuth() != null) {
            filename = filename + "_" + firebase.getAuth().getUid().replaceAll("[^a-zA-z0-9]", "_");
        }
        return filename;
    }

    public String localSaveFilename()
    {
        String filename = "mySightings.csv";
        if (firebase.getAuth() != null) {
            filename = filename + "_" + firebase.getAuth().getUid().replaceAll("[^a-zA-z0-9]", "_");
        }
        return filename;
    }

    public void showAboutUs(final Activity currentActivity)
    {
        Intent a = new Intent(currentActivity, AboutUsActivity.class);
        currentActivity.startActivity(a);
    }

    public void showEditAccount(final Activity currentActivity)
    {
        Intent a = new Intent(currentActivity, EditAccountActivity.class);
        currentActivity.startActivity(a);
    }
}
