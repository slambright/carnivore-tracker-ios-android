package org.cheetah.carnivoretracker;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class ReportSightingActivity extends AppCompatActivity {
    private Spinner mIndividualCount;
    private View mReportSightingForm;
    private View mProgressView;
    private Button mReportSightingsBtn;
    private Species mSpecies;
    private double mLatitude;
    private double mLongitude;
    private double mAltitude;
    private LocationListener mLocationListener;
    private Handler mStartUploadHandler;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_sighting);

        mReportSightingForm = findViewById(R.id.report_sighting_form);
        mProgressView = findViewById(R.id.report_sighting_progress);

        int speciesID = getIntent().getIntExtra("speciesID", 0);
        mSpecies = Species.allSpecies[speciesID];

        ImageView speciesImage = (ImageView)findViewById(R.id.reportSightingSpeciesImage);
        speciesImage.setImageResource(mSpecies.m_image);

        TextView speciesName = (TextView)findViewById(R.id.displayName);
        speciesName.setText(mSpecies.m_displayName);

        TextView altNames = (TextView)findViewById(R.id.altNames);
        altNames.setText(mSpecies.m_altNames);

        TextView iucnStatus = (TextView)findViewById(R.id.iucnStatus);
        iucnStatus.setText(mSpecies.m_iucnStatus);

        TextView habitat = (TextView)findViewById(R.id.habitat);
        habitat.setText(mSpecies.m_habitat);

        TextView diet = (TextView)findViewById(R.id.diet);
        diet.setText(mSpecies.m_diet);

        TextView size = (TextView)findViewById(R.id.size);
        size.setText(mSpecies.m_size);

        TextView weight = (TextView)findViewById(R.id.weight);
        weight.setText(mSpecies.m_weight);

        TextView socialBehavior = (TextView)findViewById(R.id.socialBehavior);
        socialBehavior.setText(mSpecies.m_socialBehavior);

        TextView speciesActivity = (TextView)findViewById(R.id.speciesActivity);
        speciesActivity.setText(mSpecies.m_activity);

        TextView interestingFact = (TextView)findViewById(R.id.interestingFact);
        interestingFact.setText(mSpecies.m_interestingFact);

        EditText freeNotes = (EditText)findViewById(R.id.txtSightingNotes);
        assert freeNotes != null;
        freeNotes.setText("");

        mIndividualCount = (Spinner)findViewById(R.id.individualCount);

        mReportSightingsBtn = (Button)findViewById(R.id.report_sighting_btn);

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        TextView webViewLabel = (TextView)findViewById(R.id.species_sightings_map_lbl);

        if (isNetworkAvailable()) {
            webView.setVisibility(View.VISIBLE);
            webViewLabel.setVisibility(View.VISIBLE);
            webView.loadUrl("https://public.tableau.com/views/CarnivoreTrackerReportedSightings/CarnivoreTrackerSightings" +
                    "?:embed=y" +
                    "&:display_count=no" +
                    "&:showTabs=n" +
                    "&Species=" + mSpecies.m_displayName);
        }
        else {
            webView.setVisibility(View.INVISIBLE);
            webViewLabel.setVisibility(View.INVISIBLE);
        }

        String[] individualCountOptions = {
                "1", "2", "3", "4", "5",
                "6", "7", "8", "9", "10",
                "11", "12", "13", "14", "15",
                "16", "17", "18", "19", "20",
                ">20"
        };
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_dropdown_item_1line,
                individualCountOptions);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mIndividualCount.setAdapter(spinnerAdapter);
        mIndividualCount.setSelection(0);

        if (Build.VERSION.SDK_INT >= 16) {
            setDropDownWidth(mIndividualCount, 300);
        }

        Button reportSightingButton = (Button) findViewById(R.id.report_sighting_btn);
        reportSightingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reportSighting();
            }
        });

        mLatitude = 0.0;
        mLongitude = 0.0;
        mAltitude = 0.0;

        mLocationListener = new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
                if (status != LocationProvider.AVAILABLE) {
                    mLatitude = 0.0;
                    mLongitude = 0.0;
                    mAltitude = 0.0;
                }
            }

            public void onProviderEnabled(String str) {

            }

            public void onProviderDisabled(String str) {
                mLatitude = 0.0;
                mLongitude = 0.0;
                mAltitude = 0.0;
            }

            public void onLocationChanged(Location location) {
                mLatitude = location.getLatitude();
                mLongitude = location.getLongitude();
                mAltitude = location.getAltitude();
            }
        };

        mStartUploadHandler = new Handler();

        getLocation();
    }

    private void getLocation() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        else {
            LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, mLocationListener);
            mReportSightingsBtn.setEnabled(true);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onPause() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.removeUpdates(mLocationListener);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        getLocation();
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();

                } else {
                    mReportSightingsBtn.setEnabled(false);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @TargetApi(16)
    private void setDropDownWidth(Spinner spinner, int width)
    {
        spinner.setDropDownWidth(width);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_report_sighting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about_us) {
            getApp().showAboutUs(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Remove all non-ASCII chars and any character that could confuse CSV parsing, replacing with an underscore ("_")
    private String CleanString(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);

            if (c < 32 || c > 255 || c == '"' || c == '\'' || c == ',' || c == ';')
                sb.append("_");
            else
                sb.append(c);
        }
        return sb.toString();
    }

    private void reportSighting() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        String data = "<SOL>,";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        data += sdf.format(new Date()) + ",";
        data += mSpecies.m_displayName + ",";
        data += mIndividualCount.getSelectedItem().toString() + ",";

        if (mLatitude == 0.0 || mLongitude == 0.0) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                mLatitude = location.getLatitude();
                mLongitude = location.getLongitude();
                mAltitude = location.getAltitude();
            }
        }

        if (mLatitude == 0.0 || mLongitude == 0.0) {
            showWaitingForLocation();
            return;
        }

        data += mLatitude + ",";
        data += mLongitude + ",";
        data += mAltitude + ",";

        String notes = ((EditText) findViewById(R.id.txtSightingNotes)).getText().toString();
        data += CleanString(notes);

        showProgress(true);

        final String finalData = data;
        Runnable r = new Runnable() {
            @Override
            public void run() {
                reportSighting(finalData);
            }
        };
        mStartUploadHandler.postDelayed(r, 50);
    }

    private void reportSighting(String data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (getApp().reportSighting(this, data)) {
            builder.setMessage(getString(R.string.action_upload_sightings)).setTitle(getString(R.string.action_upload_sightings_title));
        }
        else {
            builder.setMessage(getString(R.string.action_record_sightings)).setTitle(getString(R.string.action_record_sightings_title));
        }
        builder.setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates(mLocationListener);
    }

    private void showWaitingForLocation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.error_no_gps)).setTitle(getString(R.string.title_gps_not_ready));
        builder.setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public CarnivoreTrackerApplication getApp()
    {
        return (CarnivoreTrackerApplication )super.getApplication();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mReportSightingForm.setVisibility(show ? View.GONE : View.VISIBLE);
            mReportSightingForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mReportSightingForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mReportSightingForm.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
