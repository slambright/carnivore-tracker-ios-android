package org.cheetah.carnivoretracker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.HashMap;
import java.util.Map;


public class CreateAccountActivity extends AppCompatActivity {
    private View mCreateAccountFormView;
    private View mProgressView;
    private Button mCreateAccountButton;

    private TextView mEmailView;
    private TextView mPasswordView;
    private TextView mPasswordView2;
    private TextView mPhoneView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        mCreateAccountButton = (Button) findViewById(R.id.create_account_button);
        mCreateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptCreateAccount();
            }
        });

        mCreateAccountFormView = findViewById(R.id.create_account_form);
        mProgressView = findViewById(R.id.create_account_progress);

        mEmailView = (TextView) findViewById(R.id.create_account_email);
        mPasswordView = (TextView) findViewById(R.id.create_account_password);
        mPasswordView2 = (TextView) findViewById(R.id.create_account_password2);
        mPhoneView = (TextView) findViewById(R.id.create_account_phone);

        Intent intent = getIntent();
        String email = intent.getStringExtra("email");
        mEmailView.setText(email);

        TextView privacyLink = (TextView)findViewById(R.id.create_account_privacy_link);
        privacyLink.setMovementMethod(LinkMovementMethod.getInstance());

        String linkHtml = getText(R.string.privacy_link).toString();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            privacyLink.setText(Html.fromHtml(linkHtml, Html.FROM_HTML_MODE_LEGACY));
        } else {
            privacyLink.setText(Html.fromHtml(linkHtml));
        }

        mPasswordView.requestFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about_us) {
            getApp().showAboutUs(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void attemptCreateAccount()
    {
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mPasswordView2.setError(null);
        mPhoneView.setError(null);
        boolean tryCreate = false;

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String password2 = mPasswordView2.getText().toString();
        String phone = mPhoneView.getText().toString();

        mPasswordView2.setText("");
        View focusView = null;

        if (email.isEmpty()) {
            mEmailView.setError(getString(R.string.error_email_required));
            focusView = mEmailView;
        }
        else if (!LoginActivity.isPasswordValid(password))
        {
            mPasswordView.setError(getString(R.string.error_password_length));
            focusView = mPasswordView;
        }
        else if (!password.equals(password2)) {
            mPasswordView2.setError(getString(R.string.error_password_mismatch));
            focusView = mPasswordView2;
        }
        else if (phone.isEmpty()) {
            mPhoneView.setError(getString(R.string.error_phone_required));
            focusView = mPhoneView;
        }
        else {
            tryCreate = true;
        }

        if (!tryCreate && focusView != null)
        {
            focusView.requestFocus();
        }

        if (tryCreate)
        {
            showProgress(true);
            attemptCreateAccount(email, password, phone);
        }
    }

    private void attemptCreateAccount(final String email, final String password, final String phone)
    {
        Firebase firebase = getApp().firebase;
        firebase.createUser(email, password, new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                //LoginActivity.mEmailView.setText(email);
                //LoginActivity.mPasswordView.setText(password);
                Firebase firebase = getApp().firebase;

                firebase.authWithPassword(email, password, new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {
                        showProgress(false);
                        Map<String, String> map = new HashMap<>();
                        map.put("phone", phone);
                        getApp().mPhone = phone;
                        Firebase firebase = getApp().firebase;
                        firebase.child("users").child(authData.getUid()).setValue(map);
                        loginUser();
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError error) {
                        // Shouldn't be possible...
                        showProgress(false);
                    }
                });
            }

            @Override
            public void onError(FirebaseError error) {
                showProgress(false);
                if (error.getCode() == FirebaseError.INVALID_EMAIL) {
                    showInvalidEmailError();
                }
                else {
                    showAccountCreateError();
                }
            }
        });
    }

    private void loginUser() {
        finish();
    }

    private void showInvalidEmailError() {
        TextView emailView = (TextView) findViewById(R.id.create_account_email);
        emailView.setError(getString(R.string.error_invalid_email));
        emailView.requestFocus();
    }

    private void showAccountCreateError() {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(getString(R.string.error_create_account_failed));
        dlgAlert.setTitle(getString(R.string.title_create_account_failed));
        dlgAlert.setPositiveButton(getString(R.string.OK), null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    /**
     * Shows the progress UI and hides the form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mCreateAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mCreateAccountFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mCreateAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mCreateAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public CarnivoreTrackerApplication getApp()
    {
        return (CarnivoreTrackerApplication )super.getApplication();
    }
}
