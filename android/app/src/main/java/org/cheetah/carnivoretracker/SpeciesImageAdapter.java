package org.cheetah.carnivoretracker;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SpeciesImageAdapter extends BaseAdapter {
    private Context m_context;

    public SpeciesImageAdapter(Context context) {
        m_context = context;
    }

    public int getCount() {
        return Species.allSpecies.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout outerLayout;
        ImageView imageView;
        TextView textView;

        if (convertView == null) {
            imageView = new ImageView(m_context);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setPadding(8, 8, 8, 8);

            textView = new TextView(m_context);
            textView.setLines(2);

            outerLayout = new LinearLayout(m_context);
            //outerLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            outerLayout.addView(imageView);
            outerLayout.addView(textView);

            outerLayout.setMinimumHeight(100);
            outerLayout.setOrientation(LinearLayout.VERTICAL);
        } else {
            outerLayout = (LinearLayout) convertView;
            imageView = (ImageView)outerLayout.getChildAt(0);
            textView = (TextView)outerLayout.getChildAt(1);
        }


        // Always use the low res versions for the grid view; the scroll speed is worth it.
        BitmapFactory.Options options = new BitmapFactory.Options();

        imageView.setImageDrawable(
                m_context.getResources().getDrawableForDensity(Species.allSpecies[position].m_image,
                        DisplayMetrics.DENSITY_MEDIUM)
        );
        textView.setText(Species.allSpecies[position].m_displayName);

        return outerLayout;
    }
}
