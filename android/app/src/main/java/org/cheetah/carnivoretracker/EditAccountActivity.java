package org.cheetah.carnivoretracker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.HashMap;
import java.util.Map;

public class EditAccountActivity extends AppCompatActivity {
    private TextView mEmailAddressView;
    private TextView mCurrentPasswordView;
    private TextView mNewPasswordView;
    private TextView mNewPasswordView2;
    private TextView mPhoneView;
    private View mEditAccountView;
    private View mProgressView;
    private View mDefaultFocusView;
    private boolean mAccountUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);

        // Populate members and hookup events
        mEditAccountView = findViewById(R.id.edit_account_form);
        mProgressView = findViewById(R.id.edit_account_progress);
        mEmailAddressView = (TextView)findViewById(R.id.edit_account_email);
        mCurrentPasswordView = (TextView)findViewById(R.id.edit_account_current_password);
        mNewPasswordView = (TextView)findViewById(R.id.edit_account_new_password);
        mNewPasswordView2 = (TextView)findViewById(R.id.edit_account_new_password2);
        mPhoneView = (TextView)findViewById(R.id.edit_account_phone);

        mAccountUpdated = false;

        Button updateAccountButton = (Button) findViewById(R.id.edit_account_update_btn);
        updateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptAccountUpdate();
            }
        });

        // Set initial text values
        mEmailAddressView.setText(getApp().mEmail);
        mPhoneView.setText(getApp().mPhone);

        mCurrentPasswordView.requestFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about_us) {
            getApp().showAboutUs(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean formFilledOutReasonably() {
        String email = mEmailAddressView.getText().toString();
        String curPassword = mCurrentPasswordView.getText().toString();
        String newPassword = mNewPasswordView.getText().toString();
        String newPassword2 = mNewPasswordView2.getText().toString();
        String phone = mPhoneView.getText().toString();
        View focusView = null;

        if (email.isEmpty()) {
            mEmailAddressView.setError(getString(R.string.error_invalid_email));
        }
        else if (curPassword.isEmpty() && (
                 !newPassword.isEmpty() || email.compareTo(getApp().mEmail) != 0 ||
                 !newPassword2.isEmpty())) {
            mCurrentPasswordView.setError(getString(R.string.error_no_password));
            Log.w("UpdateAccount", "Password expected");
        }
        else if (newPassword.compareTo(newPassword2) != 0) {
            mNewPasswordView2.setError(getString(R.string.error_password_mismatch));
        }
        else if (!newPassword.isEmpty() && !LoginActivity.isPasswordValid(newPassword)) {
            mNewPasswordView.setError(getString(R.string.error_password_length));
        }
        else if (phone.isEmpty()) {
            mPhoneView.setError(getString(R.string.error_phone_required));
        }
        else {
            return true;
        }

        return false;
    }

    private void attemptAccountUpdate() {
        mEmailAddressView.setError(null);
        mCurrentPasswordView.setError(null);
        mNewPasswordView.setError(null);
        mNewPasswordView2.setError(null);
        mPhoneView.setError(null);

        mDefaultFocusView = null;
        if (mEmailAddressView.hasFocus()) mDefaultFocusView = mEmailAddressView;
        if (mCurrentPasswordView.hasFocus()) mDefaultFocusView = mCurrentPasswordView;
        if (mNewPasswordView.hasFocus()) mDefaultFocusView = mNewPasswordView;
        if (mNewPasswordView2.hasFocus()) mDefaultFocusView = mNewPasswordView2;
        if (mPhoneView.hasFocus()) mDefaultFocusView = mPhoneView;

        mAccountUpdated = false;

        if (formFilledOutReasonably()) {
            showProgress(true);
            attemptAccountUpdateEmail();
        }
        else {
            accountUpdateFailed();
        }
    }

    private void attemptAccountUpdateEmail() {
        final String newEmail = mEmailAddressView.getText().toString();
        String password = mCurrentPasswordView.getText().toString();

        // Changing email
        if (newEmail.compareTo(getApp().mEmail) != 0) {
            showProgress(true);
            if (mCurrentPasswordView.getText().toString().isEmpty()) {
                Log.w("UpdateAccount", "Password required when updating email");
                mCurrentPasswordView.setError(getString(R.string.error_no_password));
                accountUpdateFailed();
            }

            getApp().firebase.changeEmail(getApp().mEmail, password, newEmail, new Firebase.ResultHandler() {
                @Override
                public void onSuccess() {
                    mAccountUpdated = true;
                    getApp().mEmail = newEmail;
                    attemptAccountUpdatePassword();
                }

                @Override
                public void onError(FirebaseError error) {
                    // Even with valid emails, I keep getting FirebaseError.INVALID_EMAIL,
                    //    so I've disabled this functionality in the UI.
                    if (error.getCode() == FirebaseError.INVALID_PASSWORD) {
                        mCurrentPasswordView.setError(getString(R.string.error_bad_password));
                        Log.w("UpdateAccount", "Password incorrect for updating email");
                    } else {
                        mEmailAddressView.setError(getString(R.string.error_update_email_failed));
                    }
                    accountUpdateFailed();
                }
            });
        }
        else {
            attemptAccountUpdatePassword();
        }
    }

    private void attemptAccountUpdatePassword() {
        String newPassword = mNewPasswordView.getText().toString();

        if (!newPassword.isEmpty()) {
            String email = getApp().mEmail;
            String currentPassword = mCurrentPasswordView.getText().toString();

            getApp().firebase.changePassword(email, currentPassword, newPassword, new Firebase.ResultHandler() {
                @Override
                public void onSuccess() {
                    mAccountUpdated = true;
                    attemptAccountUpdatePhone();
                }

                @Override
                public void onError(FirebaseError error) {
                    if (error.getCode() == FirebaseError.INVALID_PASSWORD) {
                        mCurrentPasswordView.setError(getString(R.string.error_bad_password));
                        Log.w("UpdateAccount", "Password incorrect for changing password");
                    } else {
                        mNewPasswordView.setError(getString(R.string.error_update_password_failed));
                    }
                    accountUpdateFailed();
                }
            });
        }
        else {
            attemptAccountUpdatePhone();
        }

    }

    private void attemptAccountUpdatePhone() {
        String phone = mPhoneView.getText().toString();

        if (getApp().mPhone.compareTo(phone) != 0) {
            Map<String, String> map;
            map = new HashMap<>();
            map.put("phone", phone);
            getApp().firebase.child("users").child(getApp().firebase.getAuth().getUid()).setValue(map);
            getApp().mPhone = phone;
            mAccountUpdated = true;
            accountUpdateFinished();
        }
        else {
            accountUpdateFinished();
        }
    }

    private void accountUpdateFinished() {
        mCurrentPasswordView.setText("");
        mNewPasswordView.setText("");
        mNewPasswordView2.setText("");

        showProgress(false);

        if (mEmailAddressView.getError() != null) {
            mEmailAddressView.requestFocus();
            accountUpdateFailed();
        }
        else if (mCurrentPasswordView.getError() != null) {
            mCurrentPasswordView.requestFocus();
            accountUpdateFailed();
        }
        else if (mNewPasswordView.getError() != null) {
            mNewPasswordView.requestFocus();
            accountUpdateFailed();
        }
        else if (mNewPasswordView2.getError() != null) {
            mNewPasswordView2.requestFocus();
            accountUpdateFailed();
        }
        else if (mPhoneView.getError() != null) {
            mPhoneView.requestFocus();
            accountUpdateFailed();
        }
        else {
            // No errors... if anything was changed, then we're done.
            if (mAccountUpdated) {
                finish();
            }
            else if (mDefaultFocusView != null) {
                mDefaultFocusView.requestFocus();
            }
        }
    }

    private void accountUpdateFailed() {
        showProgress(false);
        mNewPasswordView2.setText("");
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mEditAccountView.setVisibility(show ? View.GONE : View.VISIBLE);
            mEditAccountView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mEditAccountView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mEditAccountView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public CarnivoreTrackerApplication getApp()
    {
        return (CarnivoreTrackerApplication )super.getApplication();
    }
}
