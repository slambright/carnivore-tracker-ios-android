package org.cheetah.carnivoretracker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        TextView textView = (TextView)findViewById(R.id.about_us_text);
        textView.setText(getText(R.string.about_us_text));

        TextView privacyLink = (TextView)findViewById(R.id.about_us_privacy_link);
        privacyLink.setMovementMethod(LinkMovementMethod.getInstance());

        String linkHtml = getText(R.string.privacy_link).toString();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            privacyLink.setText(Html.fromHtml(linkHtml, Html.FROM_HTML_MODE_LEGACY));
        } else {
            privacyLink.setText(Html.fromHtml(linkHtml));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about_us, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
