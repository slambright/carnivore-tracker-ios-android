package org.cheetah.carnivoretracker;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import android.content.Context;

public class Species {
    public String m_displayName;
    public String m_altNames;
    public String m_iucnStatus;
    public String m_habitat;
    public String m_diet;
    public String m_size;
    public String m_weight;
    public String m_activity;
    public String m_socialBehavior;
    public String m_interestingFact;
    public int m_image;

    private enum Columns {
        Species,
        OtherNames,
        IUCNStatus,
        Habitat,
        Diet,
        Size,
        Weight,
        Activity,
        SocialBehavior,
        InterestingFact,
        Resource
    };

    Species(String displayName,
            String altNames,
            String iucnStatus,
            String habitat,
            String diet,
            String size,
            String weight,
            String activity,
            String socialBehavior,
            String interestingFact,
            int image)
    {
        m_displayName = displayName;
        m_altNames = altNames;
        m_iucnStatus = iucnStatus;
        m_habitat = habitat;
        m_diet = diet;
        m_size = size;
        m_weight = weight;
        m_activity = activity;
        m_socialBehavior = socialBehavior;
        m_interestingFact = interestingFact;
        m_image = image;
    }

    public static Species[] allSpecies = null;

    public static void initSpecies(Context c)
    {
        if (allSpecies == null)
        {

            InputStream input = null;

            try {
                input = c.getAssets().open(c.getString(R.string.species_data_file));
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (input == null)
                return;

            InputStreamReader is = new InputStreamReader(input);
            BufferedReader br = new BufferedReader(is);
            String line;
            CSVReader cr = new CSVReader();
            ArrayList<Species> alSpecies = new ArrayList<>();
            Boolean fInferSeparator = true;
            Boolean skippedHeaders = false;


            try {
                while((line = br.readLine()) != null) {
                    if (!skippedHeaders)
                    {
                        skippedHeaders = true;
                        continue;
                    }

                    String[] row = cr.GetCSVLine(line, fInferSeparator);
                    fInferSeparator = false;    // only infer the separator for the first line.
                    // use reflection to get the imageID.
                    int imageResourceID = c.getResources().getIdentifier(row[Columns.Resource.ordinal()].replace("R.drawable.", ""), "drawable", c.getPackageName());
                    alSpecies.add(
                            new Species(
                                    row[Columns.Species.ordinal()],
                                    row[Columns.OtherNames.ordinal()],
                                    row[Columns.IUCNStatus.ordinal()],
                                    row[Columns.Habitat.ordinal()],
                                    row[Columns.Diet.ordinal()],
                                    row[Columns.Size.ordinal()],
                                    row[Columns.Weight.ordinal()],
                                    row[Columns.Activity.ordinal()],
                                    row[Columns.SocialBehavior.ordinal()],
                                    row[Columns.InterestingFact.ordinal()],
                                    imageResourceID));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            allSpecies = alSpecies.toArray(new Species[alSpecies.size()]);
        }
    }
}
