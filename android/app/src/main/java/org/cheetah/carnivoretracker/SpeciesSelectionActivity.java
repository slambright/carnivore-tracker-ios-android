package org.cheetah.carnivoretracker;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.GridView;


public class SpeciesSelectionActivity extends ActionBarActivity {

    private GridView speciesGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_species_selection);
        speciesGrid = (GridView) findViewById( R.id.gridView );
        speciesGrid.setAdapter(new SpeciesImageAdapter(this));

        speciesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                System.out.println("Selected " + Species.allSpecies[position].m_displayName);

                Intent a = new Intent(SpeciesSelectionActivity.this, ReportSightingActivity.class);
                a.putExtra("speciesID", position);
                startActivity(a);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_species_selection, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {
        getApp().mQuitApp = true;
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sign_out) {
            getApp().logout();
            finish();
            return true;
        }
        else if (id == R.id.action_about_us) {
            getApp().showAboutUs(this);
            return true;
        }
        else if (id == R.id.action_edit_account) {
            getApp().showEditAccount(this);
            return true;
        }
        else if (id == R.id.action_show_sighting) {
            Intent a = new Intent(this, ShowSightingActivity.class);
            this.startActivity(a);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getApp().uploadSightings(this)) {
            getApp().showUploadSuccessDialog(this);
        }
    }

    public CarnivoreTrackerApplication getApp()
    {
        return (CarnivoreTrackerApplication )super.getApplication();
    }
}
